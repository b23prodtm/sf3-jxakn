/*
 * Copyright 2017 www.b23prodtm.info.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sf.jiga.xtended.kernel;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author wwwb23prodtminfo <b23prodtm at sourceforge.net>
 */
public class SpritesCacheManagerTest {

        SpritesCacheManager cache;

        public SpritesCacheManagerTest() {
                cache = new SpritesCacheManager();
        }

        @BeforeClass
        public static void setUpClass() {
                Logger.getGlobal().log(Level.CONFIG, "cache directory : {0}", SpritesCacheManager._cacheDisk_dir);
        }

        @AfterClass
        public static void tearDownClass() {
        }

        @Before
        public void setUp() {
                cache.addSpritesCacheListener(new SpritesCacheAdapter() {
                        @Override
                        public void writeStarted() {
                                Logger.getGlobal().config("write started...");
                        }
                });
        }

        @After
        public void tearDown() {
                cache.clearMemorySwap();
        }

        @Test
        public void testAdd() {
                String k = "a-key", v = "test String object";
                cache.add(k, v, true);
                assertEquals(v, cache.get(k));
        }

}
