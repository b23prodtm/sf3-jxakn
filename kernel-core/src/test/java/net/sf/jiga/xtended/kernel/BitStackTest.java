/*
 * Copyright 2017 wwwb23prodtminfo <b23prodtm at sourceforge.net>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sf.jiga.xtended.kernel;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author wwwb23prodtminfo <b23prodtm at sourceforge.net>
 */
public class BitStackTest {
        
        public BitStackTest() {
        }
        
        @BeforeClass
        public static void setUpClass() {
        }
        
        @AfterClass
        public static void tearDownClass() {
        }
        
        @Before
        public void setUp() {
        }
        
        @After
        public void tearDown() {
        }

        /**
         * Test of getBitsStackCurrentLevel method, of class BitStack.
         */
        @Test
        public void testGetBitsStackCurrentLevel() {
                System.out.println("getBitsStackCurrentLevel");
                BitStack instance = new BitStack();
                int expResult = -1;
                int result = instance.getBitsStackCurrentLevel();
                assertEquals(expResult, result);
        }

        /**
         * Test of _newBitRange method, of class BitStack.
         */
        @Test
        public void test_newBitRange() {
                System.out.println("_newBitRange");
                BitStack instance = new BitStack();
                int expResult = 1;
                int result = instance._newBitRange();
                assertEquals(expResult, result);
        }

        /**
         * Test of isFull method, of class BitStack.
         */
        @Test
        public void testIsFull() {
                System.out.println("isFull");
                BitStack instance = new BitStack();
                boolean expResult = false;
                boolean result = instance.isFull();
                assertEquals(expResult, result);
        }

        /**
         * Test of _newBit method, of class BitStack.
         */
        @Test
        public void test_newBit() {
                System.out.println("_newBit");
                int range = 0;
                BitStack instance = new BitStack();
                int expResult = 1;
                int result = instance._newBit(range);
                assertEquals(expResult, result);
        }

        /**
         * Test of _getAllBitRanges method, of class BitStack.
         */
        @Test
        public void test_getAllBitRanges() {
                System.out.println("_getAllBitRanges");
                BitStack instance = new BitStack();
                int expResult = 0;
                int result = instance._getAllBitRanges();
                assertEquals(expResult, result);
        }

        /**
         * Test of _getAllBits method, of class BitStack.
         */
        @Test
        public void test_getAllBits() {
                System.out.println("_getAllBits");
                BitStack instance = new BitStack();
                int expResult = 0;
                int result = instance._getAllBits();
                assertEquals(expResult, result);
        }

        /**
         * Test of _getMask method, of class BitStack.
         */
        @Test
        public void test_getMask() {
                System.out.println("_getMask");
                int forRange = 1;
                BitStack instance = new BitStack();
                int expResult = 0;
                int result = instance._getMask(forRange);
                assertEquals(expResult, result);
        }

        /**
         * Test of _getNotMask method, of class BitStack.
         */
        @Test
        public void test_getNotMask() {
                System.out.println("_getNotMask");
                int forRange = 1;
                BitStack instance = new BitStack();
                int expResult = 0;
                int result = instance._getNotMask(forRange);
                assertEquals(expResult, result);
        }

        /**
         * Test of _toBitsString method, of class BitStack.
         */
        @Test
        public void test_toBitsString_int() {
                System.out.println("_toBitsString");
                int integer = 1;
                String expResult = "1";
                String result = BitStack._toBitsString(integer);
                assertEquals(expResult, result);
        }

        /**
         * Test of _toBitsString method, of class BitStack.
         */
        @Test
        public void test_toBitsString_int_int() {
                System.out.println("_toBitsString");
                int integer = 5;
                int fields = 4;
                String expResult = "0101";
                String result = BitStack._toBitsString(integer, fields);
                assertEquals(expResult, result);
        }

        /**
         * Test of test method, of class BitStack.
         */
        @Test
        public void testTest() {
                System.out.println("test");
                BitStack.test();
        }
        
}
