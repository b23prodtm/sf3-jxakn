package net.sf.jiga.xtended.kernel;

import java.math.BigInteger;

/**
 *
 * @author Tiana
 */
public class Level {

        /**
         *
         */
        public static Level NoDebug = new Level(BigInteger.ZERO);
    BigInteger n;

        /**
         *
         * @param levelInt
         */
        protected Level(BigInteger levelInt) {
        this.n = levelInt;
    }

        /**
         *
         * @param obj
         * @return
         */
        @Override
    public boolean equals(Object obj) {
        return obj instanceof Level ? n.equals(((Level) obj).n) : false;
    }

        /**
         *
         * @return
         */
        @Override
    public int hashCode() {
        return n.hashCode();
    }

        /**
         *
         * @param l
         * @return
         */
        public Level or(Level l) {
        return new Level(n.or(l.n));
    }

        /**
         *
         * @param l
         * @return
         */
        public Level and(Level l) {
        return new Level(n.and(l.n));
    }

        /**
         *
         * @param l
         * @return
         */
        public Level andNot(Level l) {
        return new Level(n.andNot(l.n));
    }

        /**
         *
         * @return
         */
        public Level not() {
        return new Level(n.not());
    }
}
