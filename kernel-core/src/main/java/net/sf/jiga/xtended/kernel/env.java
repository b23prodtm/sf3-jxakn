/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sf.jiga.xtended.kernel;

import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import static net.sf.jiga.xtended.kernel.JXAenvUtils._getSysBoolean;
import static net.sf.jiga.xtended.kernel.JXAenvUtils._getSysValue;
import static net.sf.jiga.xtended.kernel.JXAenvUtils.acc;

/**
 *
 * @author wwwb23prodtminfo
 */
public enum env {

    /**
     *
     */
    _OS() {
                @Override
                public String propertyName() {
                    return "os.name";
                }
            },
    /**
     *
     */
    OS_WINDOWS(_OS, 1) {
                @Override
                public String propertyValue() {
                    return "Windows";
                }
            },
    /**
     *
     */
    OS_WINDOWS_XP(_OS, 1) {
                @Override
                public String propertyValue() {
                    return "Windows XP";
                }
            },
    /**
     *
     */
    OS_WINDOWS_VISTA(_OS, 1) {
                @Override
                public String propertyValue() {
                    return "Windows Vista";
                }
            },
    /**
     *
     */
    OS_WINDOWS_7(_OS, 1) {
                @Override
                public String propertyValue() {
                    return "Windows 7";
                }
            },
    /**
     *
     */
    OS_WINDOWS_8(_OS, 1) {
                @Override
                public String propertyValue() {
                    return "Windows 8";
                }
            },
    /**
     *
     */
    OS_WINDOWS_10(_OS, 1) {
                @Override
                public String propertyValue() {
                    return "Windows 10";
                }
            },
    /**
     *
     */
    OS_MAC(_OS, 1) {
                @Override
                public String propertyValue() {
                    return "Mac OS";
                }
            },
    /**
     *
     */
    OS_LINUX(_OS, 1) {
                @Override
                public String propertyValue() {
                    return "Linux";
                }
            },
    /**
     *
     */
    OS_OTHER(_OS, new isOS(_OS) {
        @Override
        public boolean checkProperty() {
            return !OS_LINUX.isEnv() && !OS_MAC.isEnv() && !OS_WINDOWS.isEnv();
        }
    }) {
                @Override
                public String propertyValue() {
                    return "unknown";
                }
            },
    /**
     *
     */
    _JAVA(),
    /**
     *
     */
    _JAVA_VERSION() {
                @Override
                public String propertyName() {
                    return "java.version";
                }
            },
    /**
     *
     */
    JAVA_VERSION_5(_JAVA_VERSION, 1) {
                @Override
                public String propertyValue() {
                    return "1.5";
                }
            },
    /**
     *
     */
    JAVA_VERSION_6(_JAVA_VERSION, 1) {
                @Override
                public String propertyValue() {
                    return "1.6";
                }
            },
    /**
     *
     */
    JAVA_VERSION_7(_JAVA_VERSION, 1) {
                @Override
                public String propertyValue() {
                    return "1.7";
                }
            },
    /**
     *
     */
    JAVA_VERSION_8(_JAVA_VERSION, 1) {
                @Override
                public String propertyValue() {
                    return "1.8";
                }
            },
    /**
     *
     */
    JAVA_2D_OPENGL(_JAVA, 0) {
                @Override
                public String propertyName() {
                    return "sun.java2d.opengl";
                }

                @Override
                public String propertyValue() {
                    return "true";
                }
            },
    /**
     *
     */
    JAVA_2D_D3D(_JAVA, 0) {
                @Override
                public String propertyName() {
                    return "sun.java2d.d3d";
                }

                @Override
                public String propertyValue() {
                    return "true";
                }
            },
    /**
     *
     */
    _THREADING(),
    /**
     *
     */
    THREADING_MULTI(_THREADING, new isEnv(_THREADING) {
        @Override
        public boolean checkProperty() {
            return Runtime.getRuntime().availableProcessors() > 1 && !_getSysBoolean("jxa.nomt");
        }
    }),
    /**
     *
     */
    _ARCH() {
                @Override
                public String propertyName() {
                    return "os.arch";
                }
            },
    /**
     *
     */
    ARCH_X86(_ARCH, 0) {
                @Override
                public String propertyValue() {
                    return "x86";
                }
            },
    /**
     *
     */
    ARCH_AMD64(_ARCH, 0) {
                @Override
                public String propertyValue() {
                    return "amd64";
                }
            },
    /**
     *
     */
    ARCH_PPC(_ARCH, 0) {
                @Override
                public String propertyValue() {
                    return "ppc";
                }
            },
    /**
     *
     */
    ARCH_I386(_ARCH, 0) {
                @Override
                public String propertyValue() {
                    return "i386";
                }
            },
    /**
     *
     */
    ARCH_PPC64(_ARCH, 0) {
                @Override
                public String propertyValue() {
                    return "ppc64";
                }
            },
    /**
     *
     */
    _APP() {
                @Override
                public String propertyName() {
                    return "jxa.app.type";
                }
            },
    /**
     *
     */
    APP_APPLET(_APP, 0) {
                @Override
                public String propertyValue() {
                    return "antapplet";
                }
            },
    /**
     *
     */
    APP_FRAME(_APP, 0) {
                @Override
                public String propertyValue() {
                    return "antframe";
                }
            },
    /**
     * class loading related
     */
    APP_REMOTE(_APP, 0) {
                @Override
                public String propertyName() {
                    return "jxa.app.remote";
                }

                @Override
                public String propertyValue() {
                    return "true";
                }
            };

    private static abstract class isEnv {

        private static BitStack _ENV_BITS = new BitStack();
        env e;

        public isEnv(env e) {
            this.e = e;
        }

        public final boolean check() {
            return AccessController.doPrivileged(new PrivilegedAction<Boolean>() {
                public Boolean run() {
                    return checkProperty();
                }
            }, acc);
        }

        protected abstract boolean checkProperty();
    }

    private static class isOS extends isEnv {

        public isOS(env e) {
            super(e);
        }

        public boolean checkProperty() {
            String s = JXAenvUtils._getSysValue(e.propertyName());
            return s == null ? false : s.startsWith(e.propertyValue());
        }
    }
    /**
     * associated bitMask mask
     */
    int mask;
    isEnv check;
    env parentEnv = null;

    /**
     * @param useParentPropertyName use property name of the parent option to
     * make checks
     * @param check option {@link envCheckStartsWith} to use
     * propertyName.startwith(propertyValue)
     */
    env(final env parentEnv, int check) {
        this.parentEnv = parentEnv;
        mask = isEnv._ENV_BITS._newBit(parentEnv.mask);
        switch (check) {
            case 1:
                this.check = new isOS(this);
                break;
            default:
                this.check = new isEnv(this) {
                    @Override
                    protected boolean checkProperty() {
                        String s = JXAenvUtils._getSysValue(e.propertyName());
                        return s == null ? false : s.equalsIgnoreCase(e.propertyValue());
                    }
                };
                break;
        }
    }

    env(env parentRangeOption, isEnv check) {
        this.check = check;
        parentEnv = parentRangeOption;
        mask = isEnv._ENV_BITS._newBit(parentRangeOption.mask);

    }

    /**
     * new range of options
     */
    env() {
        parentEnv = null;
        this.check = new isEnv(this) {
            @Override
            public boolean checkProperty() {
                return true;
            }
        };
        mask = isEnv._ENV_BITS._newBitRange();
    }

    /**
     *
     * @return
     */
    public boolean isEnv() {
        return check.check();
    }

    /**
     *
     * @return
     */
    public int bitMask() {

        return mask;

    }

    /**
     *
     * @return
     */
    public int compareMask() {
        return isEnv._ENV_BITS._getAllBits() & mask;
    }

    /**
     *
     * @return
     */
    public String osname() {
        return check instanceof isOS ? propertyValue() : _getSysValue("os.name");
    }

    /**
     * the exact property name or a simple name String for this property enum
     *
     * @return
     */
    public String propertyName() {
        return parentEnv != null ? parentEnv.propertyName() : toString();
    }

    /**
     * return the current value or the expected value for this property
     *
     * @return
     */
    public String propertyValue() {
        return _getSysValue(propertyName());
    }

    /**
     * by inputing {@linkplain env} constants, this checks if ALL of the
     * constants are found in the current environment.
     *
     * @param envMask
     * @return true if all of the array is {@linkplain env#isEnv()} == true
     */
    public static boolean _isEnv(env[] envMask) {
        for (env e : envMask) {
            if (!e.isEnv()) {
                return false;
            }
        }
        return true;
    }

    /**
     * by inputing a {@linkplain env#bitMask()} bitwise OR-ed mask, this checks
     * if ALL of the bits are found.
     *
     * @param envBitsMask
     * @return
     * @discussion this method returns faster as it checks bit-values.
     */
    public static boolean _isEnv(int envBitsMask) {
        return _isEnv(envBitsMask, envBitsMask);
    }

    /**
     * by inputing a {@linkplain env#bitMask()} bitwise OR-ed mask, this checks
     * if SOME of the bits are found.
     *
     * @param envBitsMask
     * @return
     * @discussion this method returns faster as it checks bit-values.
     */
    public static boolean _hasEnv(int envBitsMask) {
        return !_isEnv(envBitsMask, 0);
    }

    /**
     * by inputing a {@linkplain env#bitMask()} bitwise OR-ed mask, this checks
     * if envBitsMask matches ALL ref bits.
     *
     * @return true if and only if envBitsMask matches ALL ref for the current
     * environment.
     * @param envBitsMask bits to check
     * @param ref bits that must be ALL checked for in envBitsMask
     * @discussion this method returns faster as it checks bit-values.
     */
    public static boolean _isEnv(int envBitsMask, int ref) {
        return (envBitsMask & _getEnvBits() & isEnv._ENV_BITS._getAllBits()) == (ref & isEnv._ENV_BITS._getAllBits());
    }

    /**
     * returns the current runtime environment bits from {@linkplain #env} enum
     *
     * @return
     */
    public static int _getEnvBits() {
        int bits = 0;
        for (env e : env.values()) {
            if (e.isEnv()) {
                bits |= e.bitMask();
            }
        }
        return bits;
    }

    /**
     * returns the current runtime environment bits from {@linkplain env} enum
     *
     * @return
     */
    public static env[] _getEnv() {
        ArrayList<env> bits = new ArrayList<env>();
        for (env e : env.values()) {
            if (e.isEnv()) {
                bits.add(e);
            }
        }
        return bits.toArray(new env[]{});
    }

        /**
         *
         * @param property
         * @return
         */
        public static env[] _getEnv(env property) {
        ArrayList<env> bits = new ArrayList<env>();
        for (env e : env.values()) {
            if (e.isEnv() && (0 != (isEnv._ENV_BITS._getMask(property.bitMask()) & e.bitMask()))) {
                bits.add(e);
            }
        }
        return bits.toArray(new env[]{});
    }

}
