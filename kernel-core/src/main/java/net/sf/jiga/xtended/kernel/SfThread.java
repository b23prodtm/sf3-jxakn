/* * To change this template, choose Tools | Templates * and open the template in the editor. */package net.sf.jiga.xtended.kernel;/** * This class wraps the Thread class to make better identification in CoalescedThreadsMonitor stacks. * @see CoalescedThreadsMonitor * @author www.b23prodtm.info */public class SfThread {    /** the wrapped Thread instance*/    Thread t;    /** creates a new instance     @param t the Thread instance to wrap*/    

        /**
         * creates a new instance     @param t the Thread instance to wrap
         * @param t
         */
        public SfThread(Thread t) {        this.t = t;    }    /** returns the hash-code id of the wrapped Thread.     * @return the id of this Thread is returned     @see Thread#getId()*/    

        /**
         * returns the hash-code id of the wrapped Thread.     * @return the id of this Thread is returned     @see Thread#getId()
         * @return
         */
        public int hashCode() {        return (int) t.getId();    }        /** returns true or false whether this instance wraps the same Thread as the specified SfThread or not, resp.     @return true or false whether this instance wraps          * @param othe same Thread as the specified SfThread instance or not, resp.     @see #hashCode()*/    

        /**
         * returns true or false whether this instance wraps the same Thread as the specified SfThread or not, resp.@return true or false whether this instance wraps          * @param othe same Thread as the specified SfThread instance or not, resp.     @see #hashCode()
         * @param o
         * @return 
         */
        public boolean equals(Object o) {        return o instanceof SfThread ? o.hashCode() == hashCode() : false;    }    /** returns the wrapped Thread instance*/    

        /**
         * returns the wrapped Thread instance
         * @return
         */
        public Thread getThread() {        return t;    }}