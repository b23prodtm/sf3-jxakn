package net.sf.jiga.xtended.kernel;

/**
 *
 * @author Tiana
 */
public class LongLevel {

        /**
         *
         */
        public static LongLevel NoDebug = new LongLevel(0L);
    long n;

        /**
         *
         * @param levelInt
         */
        protected LongLevel(long levelInt) {
        this.n = levelInt;
    }

        /**
         *
         * @param obj
         * @return
         */
        @Override
    public boolean equals(Object obj) {
        return obj instanceof LongLevel ? n == ((LongLevel) obj).n : false;
    }

        /**
         *
         * @return
         */
        @Override
    public int hashCode() {
        return ("" + n).hashCode();
    }

        /**
         *
         * @param l
         * @return
         */
        public LongLevel or(LongLevel l) {
        return new LongLevel(n | l.n);
    }

        /**
         *
         * @param l
         * @return
         */
        public LongLevel and(LongLevel l) {
        return new LongLevel(n & l.n);
    }

        /**
         *
         * @param l
         * @return
         */
        public LongLevel andNot(LongLevel l) {
        return new LongLevel(n & ~l.n);
    }

        /**
         *
         * @return
         */
        public LongLevel not() {
        return new LongLevel(~n);
    }
}
