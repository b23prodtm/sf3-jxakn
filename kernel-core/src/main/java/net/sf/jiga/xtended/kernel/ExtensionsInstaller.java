/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sf.jiga.xtended.kernel;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import net.sf.jiga.xtended.JXAException;
import static net.sf.jiga.xtended.kernel.FileHelper.FILE_READ;
import static net.sf.jiga.xtended.kernel.FileHelper.FILE_WRITE;
import static net.sf.jiga.xtended.kernel.FileHelper._TMPDIRECTORY;
import static net.sf.jiga.xtended.kernel.FileHelper._USERHOMEDIRECTORY;
import static net.sf.jiga.xtended.kernel.JXAenvUtils.*;
import net.sf.jiga.xtended.kernel.JXAenvUtils.LVL;
import static net.sf.jiga.xtended.kernel.env.JAVA_VERSION_5;
import static net.sf.jiga.xtended.kernel.env.JAVA_VERSION_6;
import static net.sf.jiga.xtended.kernel.env.JAVA_VERSION_7;
import static net.sf.jiga.xtended.kernel.env.OS_LINUX;
import static net.sf.jiga.xtended.kernel.env.OS_MAC;
import static net.sf.jiga.xtended.kernel.env.OS_WINDOWS;
import static net.sf.jiga.xtended.kernel.env.OS_WINDOWS_XP;
import static net.sf.jiga.xtended.kernel.env._hasEnv;
import net.sf.jiga.xtended.ui.AntHandler;
import net.sf.jiga.xtended.ui.Display;
import net.sf.jiga.xtended.ui.UIMessage;

/**
 * Extensions Toolkit. the main function runs a JXAenvUtils instance to load all
 * the required natives. it's not worth to call it locally (if you want to do
 * so, prefer the
 * {@linkplain AntHandler#_load(net.sf.jiga.xtended.ui.Ant, net.sf.jiga.xtended.ui.DisplayInterface, java.util.Map, java.util.List)}!).
 * Please, edit build-youros-yourarch.xml for "extension.properties" !
 *
 * @author www.b23prodtm.info
 */
public class ExtensionsInstaller {

        /**
         * installs all the file-map into the
         * {@link #_findExtPath(boolean) extension path}. It is used to load
         * extra files into the extensions path. All write permissions must have
         * been granted before
         *
         * @param <U>
         * @param map
         * @param libpath assumes that the map is with U<Map<URL, Boolean>> if
         * true. the map denotes a set of <b>native</b> libraries
         * (.dll,.so,.jnilib,.dyllib) that are loaded @param map a map of all
         * resources files to install into {@link #_findExtPath(boolean)
         * extension path}with the following struct. : Map<OSNAME, Map<uniqueRESID,
         * accessibleRESfilename>> the accessibleRESfilename must target an
         * accessible resource file OSNAME denotes a String identifying an
         * os.name or the starting String of os.name, e.g. "Windows" can stand
         * for Windows XP and Windows Vista      <pre>
         * Map<String, Map<String, URL>> map = new LinkedHashMap<String, Map<String, URL>>();
         * Map<String, String> exts;
         * map.put("Mac OS", exts = new LinkedHashMap<String, URL>());
         * exts.put("lwjgl", new URL("/natives/liblwjgl.jnilib"));
         * _installExtensions(map, true);
         * </pre>
         *
         * * @param backupDirectory if set, it will first look if a recent
         * backup has been found or get files from source env.
         * @param backupDirectory
         * @return 
         * @throws java.net.MalformedURLException 
         * @see AntHandler#_load(net.sf.jiga.xtended.ui.Ant,
         * net.sf.jiga.xtended.ui.DisplayInterface, java.util.Map,
         * java.util.List)
         * @see #_uninstallExtensions(net.sf.jiga.xtended.kernel.JXAenvUtils,
         * java.util.Map, boolean, java.io.File)
         */
        public static <U> JXAenvUtils _installExtensions(Map<String, Map<String, U>> map, boolean libpath, File backupDirectory) throws MalformedURLException {
                JXAenvUtils env = new JXAenvUtils();
                env.setJXAenvPath(_findExtPath(libpath));
                env.setBigBuffer(true);
                return _installExtensions(env, map, libpath, backupDirectory);

        }

        /**
         * uninstalls from the JXA env.
         *
         * @param <U>
         * @param map
         * @param backupDirectory if set it is where the removed files will be
         * backed up
         * @param libpath
         * @return 
         */
        public static <U> JXAenvUtils _uninstallExtensions(Map<String, Map<String, U>> map, boolean libpath, File backupDirectory) {
                JXAenvUtils env = new JXAenvUtils();
                env.setJXAenvPath(_findExtPath(libpath));
                return _uninstallExtensions(env, map, libpath, backupDirectory);
        }

        /**
         * finds an available extension path for the current runtime and, if
         * specified, checks for the availability of an extension path that can
         * load natives packages. java.library.path will be checked if the
         * "libpath" arg is set "true".<br> the java.ext.dirs lookup can be
         * en/disabled : it's especially on non-OS X machines that it is
         * required. Windows and Linux systems have it disabled by default (OS X
         * ext.dirs is disabled since JXA 4.4, too), to enable or disable :
         * simply set <b>property
         * <u>jxa.ext.dirs=true,false</u></b> at launching.
         *
         * @param libpath if true, look into the java.library.path for the
         * existence of an extension directory
         * @return a pathname to an available extension path
         */
        public static String _findExtPath(final boolean libpath) {
                return AccessController.doPrivileged(new PrivilegedAction<String>() {
                        public String run() {
                                return __findExtPath(libpath);
                        }
                });
        }

        private static String __findExtPath(boolean libpath) {
                boolean writeableExts = false;
                String extPath = "---";
                String exts = libpath ? getLibraryPath() : _getSysValue("java.ext.dirs");
                boolean useSystemExtPaths = Boolean.parseBoolean(rb.getString("useSystemExtensionsPaths")) || _getSysBoolean("jxa.ext.dirs");
                if (_debugSys) {
                        System.out.println(log("Detecting your home (writeable) folder : " + FileHelper._USERHOMEDIRECTORY.getAbsolutePath(), LVL.SYS_NOT));
                        System.out.println(log("Detecting your temp (writeable) folder : " + FileHelper._TMPDIRECTORY.getAbsolutePath(), LVL.SYS_NOT));
                        System.out.println(log("Detecting your class-path : " + getClasspath(), LVL.SYS_NOT));
                        System.out.println(log("Detecting your extensions path : " + exts, LVL.SYS_NOT));
                }
                if (libpath) {
                        if (_debugSys) {
                                System.out.println(log("Detecting your Library path : " + getLibraryPath(), LVL.SYS_NOT));
                        }
                }
                /**
                 * find writeable path
                 */
                if (useSystemExtPaths) {
                        if (OS_WINDOWS.isEnv()) {
                                if (!_hasEnv(OS_WINDOWS_XP.bitMask())) {
                                        String dir = JAVA_VERSION_7.isEnv() ? "jre7" : JAVA_VERSION_6.isEnv() ? "jre6" : "jre5";
                                        exts = _updatePath(exts, FileHelper._USERHOMEDIRECTORY.getPath() + File.separator + "AppData\\Local\\VirtualStore\\Program Files\\Java\\" + dir + "\\lib\\ext");
                                }
                        }
                        for (String path : exts.split(File.pathSeparator)) {
                                if (writeableExts = FileHelper._accessFilePermitted(new File(path), FILE_READ | FILE_WRITE)) {
                                        extPath = path;
                                        break;
                                }
                        }
                }
                /**
                 * define a library.path outside ext path if noting could be
                 * found from ext path (non-OSX specific) if (useSystemExtPaths)
                 * { if (!writeableExts && libpath) { for (String libDir :
                 * getLibraryPath().split(File.pathSeparator)) { if
                 * (writeableExts = _accessFilePermitted(new File(libDir),
                 * FILE_READ | FILE_WRITE)) { extPath = libDir; break; } } } }
                 */
                if (!writeableExts && !FileHelper._USERHOMEDIRECTORY.equals(new File("."))) {
                        if (writeableExts = FileHelper._accessFilePermitted(_USERHOMEDIRECTORY, FILE_READ | FILE_WRITE)) {
                                extPath = FileHelper._USERHOMEDIRECTORY.getAbsolutePath();
                        }
                }
                if (!writeableExts && !FileHelper._TMPDIRECTORY.equals(new File("."))) {
                        if (writeableExts = FileHelper._accessFilePermitted(_TMPDIRECTORY, FILE_READ | FILE_WRITE)) {
                                extPath = FileHelper._TMPDIRECTORY.getAbsolutePath();
                        }
                }
                if (!writeableExts) {
                        extPath = ".";
                }
                if (libpath) {
                        if (_debugSys) {
                                System.out.println(JXAenvUtils.log("Altering java.library.path may not be successful", LVL.SYS_WRN));
                        }
                        _setSysValue("java.library.path", _updatePath(getLibraryPath(), extPath));
                }
                if (_debugSys) {
                        System.out.println(log("Using " + (libpath ? "lib." : "ext.") + " path : " + extPath, LVL.SYS_NOT));
                }
                return extPath;
        }

        /**
         *
         */
        public static boolean showSplash = false;

        /**
         *
         */
        public static URL splashPic = ExtensionsInstaller.class.getResource("/net/sf/jiga/xtended/ui/images/Sf3Splash.png");

        /**
         * installs all the file-map into the
         * {@link #_findExtPath(boolean) extension path}. It is used to load
         * extra files into the extensions path. All write permissions must have
         * been granted before
         *
         * @param <U>
         * @param env if you already have instanciated an env it can be used
         * here
         * @param map
         * @param libpath assumes that the map is with U<Map<URL, Boolean>> if
         * true. the map denotes a set of <b>native</b> libraries
         * (.dll,.so,.jnilib,.dyllib) that are loaded @param map a map of all
         * resources files to install into {@link #_findExtPath(boolean)
         * extension path} with the following struct. : Map<OSNAME, Map<uniqueRESID,
         * accessibleRESfilename>> the accessibleRESfilename must target an
         * accessible resource file OSNAME denotes a String identifying an
         * os.name or the starting String of os.name, e.g. "Windows" can stand
         * for Windows XP and Windows Vista      <pre>
         * Map<String, Map<String, URL>> map = new LinkedHashMap<String, Map<String, URL>>();
         * Map<String, String> exts;
         * map.put("Mac OS", exts = new LinkedHashMap<String, URL>());
         * exts.put("lwjgl", new URL("/natives/liblwjgl.jnilib"));
         * _installExtensions(map, true);
         * </pre>
         *
         * * @param backupDirectory if set, it will first look if a recent
         * backup has been found or get files from source env.
         * @param backupDirectory
         * @return 
         * @throws java.net.MalformedURLException 
         * @see AntHandler#_load(net.sf.jiga.xtended.ui.Ant,
         * net.sf.jiga.xtended.ui.DisplayInterface, java.util.Map,
         * java.util.List)
         * @see #_uninstallExtensions(net.sf.jiga.xtended.kernel.JXAenvUtils,
         * java.util.Map, boolean, java.io.File)
         */
        public static <U> JXAenvUtils _installExtensions(final JXAenvUtils env, final Map<String, Map<String, U>> map, final boolean libpath, final File backupDirectory) throws MalformedURLException {
                try {
                        return AccessController.doPrivileged(new PrivilegedExceptionAction<JXAenvUtils>() {
                                public JXAenvUtils run() throws MalformedURLException, CloneNotSupportedException {
                                        /**
                                         * LOAD backup files if they exist
                                         */
                                        if (backupDirectory instanceof File) {
                                                JXAenvUtils backenv = (JXAenvUtils) env.clone();
                                                backenv.silent = true;
                                                System.out.println(log("Loading backup " + (libpath ? "natives" : "extensions") + " (if available)...", LVL.SYS_NOT));
                                                if (libpath) {
                                                        __installExtensions(backenv, __baseFileMapNat(map, backupDirectory.toURI().toURL(), false), libpath);
                                                } else {
                                                        __installExtensions(backenv, __baseFileMapExt(map, backupDirectory.toURI().toURL(), false), libpath);
                                                }
                                                /*
                         * VOID if (backenv.isResourceLoaded()) { return
                         * backenv; }
                                                 */
                                        }
                                        System.out.println(log("Loading updated " + (libpath ? "natives" : "extensions") + " (if available)...", LVL.SYS_NOT));
                                        /**
                                         * make sure latest version is there
                                         */
                                        return __installExtensions(env, map, libpath);
                                }
                        });
                } catch (PrivilegedActionException ex) {
                        if (ex.getException() instanceof CloneNotSupportedException) {
                                if (_debugSys) {
                                        ex.getException().printStackTrace();
                                }
                                return env;
                        } else {
                                throw (MalformedURLException) ex.getException();
                        }
                }
        }

        /**
         * @param libpath assumes that the map is with U<Map<URL, Boolean>>
         */
        private static <U> JXAenvUtils __installExtensions(JXAenvUtils env, Map<String, Map<String, U>> map, boolean libpath) {
                if (map == null) {
                        System.out.println(log("Nothing loaded for extensions here.", LVL.SYS_WRN));
                        return env;
                }
                if (map.isEmpty()) {
                        System.out.println(log("Nothing loaded for extensions here.", LVL.SYS_WRN));
                        return env;
                }
                if (!_getSysValue("os.name").startsWith(rb.getString("os"))) {
                        _popExceptionToUser(true, Thread.currentThread(), new UnsupportedOperationException("The running OS " + _getSysValue("os.name") + " does not match with " + rb.getString("os")));
                        System.exit(1);
                }
                Map<String, U> extsMap = null;
                synchronized (map) {
                        for (String os : map.keySet()) {
                                if (_getSysValue("os.name").startsWith(os)) {
                                        extsMap = map.get(os);
                                        break;
                                }
                        }
                }
                /**
                 * FIND resources files as specified in the Map by the resource
                 * loader
                 */
                if (extsMap instanceof Map) {
                        synchronized (extsMap) {
                                for (String id : extsMap.keySet()) {
                                        if (libpath) {
                                                env.addEnvFile(id, ((Map<URL, Boolean>) extsMap.get(id)).keySet().iterator().next());
                                        } else {
                                                env.addEnvFile(id, (URL) extsMap.get(id));
                                        }
                                }
                        }
                        if (!env.silent && showSplash && ThreadWorks.Swing.isEventDispatchThread()) {
                                try {
                                        env.setSplashPicture(Display._Display(splashPic, null));
                                } catch (Exception ex) {
                                        if (_debugSys) {
                                                ex.printStackTrace();
                                        }
                                } finally {
                                        env.loadResource();
                                }
                        } else {
                                env.loadAll();
                        }
                        if (env.hasLoadErrors()) {
                                if (!env.silent) {
                                        String errors = "";
                                        for (String id : env.getErroredFiles()) {
                                                errors += id + " expected path : " + env.getEnvSourceFile(id) + File.pathSeparator + " not ";
                                        }
                                        String message = (libpath ? "Native libraries" : "Jar resources") + " were not found on classpath." + Console.newLine + (libpath ? "" : "Maybe <param name='keepremote' value='true'> was missing.") + " Set jxa.debugSys=true jxa.debugECL=true to show more info. " + Console.newLine + errors;
                                        if (Boolean.getBoolean("jxa.jwrapper")) {
                                                Logger.getLogger(JXAenvUtils.LOGGER_NAME).log(java.util.logging.Level.SEVERE, message);
                                        } else {
                                                throw new JXAException(JXAException.LEVEL.SYSTEM, message);
                                        }
                                }
                        } else if (libpath) {
                                /**
                                 * LOAD native files from System loader with our
                                 * ClassLoader
                                 */
                                synchronized (extsMap) {
                                        ClassLoader cL = _switchToClassLoader(env.getEnvClassLoader());
                                        /**
                                         * we set it as the first ClassLoader in
                                         * the loading chain Fixed issue :
                                         * {@link https://bitbucket.org/b23prodtm/es-sdk-jdk-binding/issues/4/load-library-from-ecl-context#comment-22821446}
                                         */
                                        ExtensionsClassLoaderImpl.isFirst.begin(true);
                                        for (String id : extsMap.keySet()) {
                                                if (!((Map<URL, Boolean>) extsMap.get(id)).values().iterator().next()) {
                                                        /**
                                                         * extNatives was
                                                         * libname:libraryfilename(:false)
                                                         * (boolean)
                                                         */
                                                        continue;
                                                }
                                                /**
                                                 * extNatives was
                                                 * libname:libraryfilename:true
                                                 * (boolean)
                                                 */
                                                try {
                                                        if (_debugSys) {
                                                                System.out.print("System.loadLibrary " + id);
                                                        }
                                                        System.loadLibrary(id);
                                                        if (_debugSys) {
                                                                System.out.println(" OK");
                                                        }
                                                } catch (Throwable e) {
                                                        if (_debugSys) {
                                                                System.out.println(" FAILED " + e.getMessage());
                                                        }
                                                        String fullPath = env.getEnvInstalledFile(id).getAbsolutePath();
                                                        if (_debugSys) {
                                                                System.out.print("System.load library " + id + " from " + fullPath);
                                                        }
                                                        try {
                                                                System.load(fullPath);
                                                                if (_debugSys) {
                                                                        System.out.println(" OK");
                                                                }
                                                        } catch (Throwable ex) {
                                                                if (_debugSys) {
                                                                        System.out.println(" FAILED " + ex.getMessage());
                                                                }
                                                                if (!env.silent) {
                                                                        throw new JXAException(e.getLocalizedMessage(), ex);
                                                                }
                                                        }
                                                }
                                        }
                                        ExtensionsClassLoaderImpl.isFirst.end();
                                        _switchToClassLoader(cL);
                                }
                        }
                }
                if (env.isResourceLoaded()) {
                        if (_debugSys) {
                                System.out.println(log("Loaded all environment resources !", LVL.SYS_NOT));
                        }
                } else if (!env.silent) {
                        System.err.println(log("Some of the environment resources failed to load. Use jxa.debugSys=true system property to see details !", LVL.SYS_ERR));
                }
                return env;
        }

        /**
         * uninstalls from the specified env.
         *
         * @param <U>
         * @param env
         * @param backupDirectory if set, it is where the removed files will be
         * backed up
         * @param map
         * @param libpath
         * @return 
         */
        public static <U> JXAenvUtils _uninstallExtensions(final JXAenvUtils env, final Map<String, Map<String, U>> map, final boolean libpath, final File backupDirectory) {
                return AccessController.doPrivileged(new PrivilegedAction<JXAenvUtils>() {
                        public JXAenvUtils run() {
                                if (backupDirectory instanceof File) {
                                        Map<String, U> osfileMap = map.get(_getSysValue("os.name"));
                                        synchronized (osfileMap) {
                                                int i = 0;
                                                for (String id : osfileMap.keySet()) {
                                                        try {
                                                                if (libpath) {
                                                                        File dst = new File(backupDirectory + File.separator + new File(((Map<URL, Boolean>) osfileMap.get(id)).keySet().iterator().next().toString()).getName());
                                                                        URL file = ((Map<URL, Boolean>) osfileMap.get(id)).keySet().iterator().next();
                                                                        FileHelper._fileCopy(file, dst, false, true);
                                                                } else {
                                                                        File dst;
                                                                        URL u = (URL) osfileMap.get(id);
                                                                        if (_debugSys) {
                                                                                System.out.println("extensions .jar file : " + u);
                                                                        }
                                                                        dst = new File(backupDirectory + File.separator + new File(u.toString()).getName());
                                                                        FileHelper._fileCopy((URL) osfileMap.get(id), dst, false, true);
                                                                }
                                                        } catch (Exception ex) {
                                                                if (_debugSys) {
                                                                        ex.printStackTrace();
                                                                }
                                                        }
                                                }
                                        }
                                }
                                return __uninstallExtensions(env, map, libpath);
                        }
                });
        }

        private static <U> JXAenvUtils __uninstallExtensions(JXAenvUtils env, Map<String, Map<String, U>> map, boolean libpath) {
                if (map == null) {
                        System.out.println(log("Nothing loaded for extensions here.", LVL.SYS_WRN));
                        return env;
                }
                if (map.isEmpty()) {
                        System.out.println(log("Nothing loaded for extensions here.", LVL.SYS_WRN));
                        return env;
                }
                synchronized (map) {
                        for (String os : map.keySet()) {
                                Map<String, U> extsMap = map.get(os);
                                if (_getSysValue("os.name").startsWith(os)) {
                                        synchronized (extsMap) {
                                                for (String id : extsMap.keySet()) {
                                                        if (libpath) {
                                                                env.addEnvFile(id, ((Map<URL, Boolean>) extsMap.get(id)).keySet().iterator().next());
                                                        } else {
                                                                env.addEnvFile(id, (URL) extsMap.get(id));
                                                        }
                                                }
                                        }
                                        if (showSplash && ThreadWorks.Swing.isEventDispatchThread()) {
                                                env.clearResource();
                                        } else {
                                                env.unloadAll();
                                        }
                                        if (env.hasLoadErrors()) {
                                                System.err.println(log("environment unloading process had error(s) on unloading", LVL.SYS_ERR));
                                        }
                                        break;
                                }
                        }
                }
                if (!env.isResourceLoaded()) {
                        if (_debugSys) {
                                System.out.println(log("Successfully unloaded all environment resources !", LVL.SYS_NOT));
                        }
                } else {
                        System.err.println(log("Some of the environment resources failed to unload.", LVL.SYS_ERR));
                }
                return env;
        }
        /**
         * resource properties
         * (resources)/os.name/all/net/sf/jiga/xtended/kernel/extension.properties
         */
        public static ResourceBundle rb = ResourceBundle.getBundle("net.sf.jiga.xtended.kernel.extension", Locale.getDefault(), ExtensionsInstaller.class.getClassLoader());

        /**
         * env map for JXA natives that can be loaded using
         * {@linkplain #_installExtensions(java.util.Map, boolean, java.io.File)}
         *
         * @param map a map to store the files mapping or null and a map will be
         * created and filled with the JXA env files
         * @param base a base URL that will be used as the base path for env
         * files in the returned map
         * @return 
         * @throws java.net.MalformedURLException
         * @throws java.net.URISyntaxException
         */
        public static Map<String, Map<String, Map<URL, Boolean>>> _getJXANatenvFiles(Map<String, Map<String, Map<URL, Boolean>>> map, URL base) throws MalformedURLException, URISyntaxException {
                return __baseFileMapNat(_getJXANatenvFiles(map, true), base, false);
        }

        /**
         * @param map a map to add the files mapping in or null and a map will
         * be created and filled with the JXA env files (into map key with the
         * name System.property("os.name"))
         * @param local use URL (false) or File (true) to find native resources;
         * by local (File) it will look in the current folder (/.), by default
         * JXA projects loads every native resources from .jar!/ URLS.
         * @return 
         * @throws java.net.MalformedURLException
         * @throws java.net.URISyntaxException
         */
        public static Map<String, Map<String, Map<URL, Boolean>>> _getJXANatenvFiles(Map<String, Map<String, Map<URL, Boolean>>> map, boolean local) throws MalformedURLException, URISyntaxException {
                map = map instanceof Map ? map : Collections.synchronizedMap(new LinkedHashMap<String, Map<String, Map<URL, Boolean>>>());
                Map<String, Map<URL, Boolean>> exts = map.get(_getSysValue("os.name"));
                if (exts == null) {
                        map.put(_getSysValue("os.name"), exts = new LinkedHashMap<String, Map<URL, Boolean>>());
                }
                String natives = rb.getString("extNatives");
                /**
                 * usually ExtensionsClassLoaderImpl
                 */
                ClassLoader extLoader = ExtensionsClassLoader.getInstance().getClassLoader();

                /*
         * rea from the extNatives property in build-os-arch.xml (and found further in extnsions.properties
                 */
                for (String nat : natives.split("\\s")) {
                        String libname = nat.split(":")[0];
                        String libfile = nat.split(":")[1];
                        boolean loadLib = nat.split(":").length == 3 ? Boolean.parseBoolean(nat.split(":")[2]) : false;
                        while (libfile.startsWith("/") && libfile.length() > 1) {
                                libfile = libfile.substring(1);
                        }
                        URL url = local ? new File("." + File.separator + libfile).toURI().toURL() : extLoader.getResource(libfile);
                        exts.put(libname, Collections.singletonMap(url, loadLib));
                }
                if (_debugSys) {
                        System.out.println("ExtensionsInstaller (natives) :: current running directory is : " + new File("."));
                }
                return map;
        }

        private static Map<String, URL> _getJXAExtFile(String jar, URL base) throws MalformedURLException {
                while (jar.startsWith("/") && jar.length() > 1) {
                        jar = jar.substring(1);
                }
                URL url = new URL(base, jar);
                return Collections.singletonMap(FileHelper._getURLFilename(url), url);
        }

        /**
         * @param map a map to add the files mapping (i.e. where the .jar
         * resources files are) or null and a map will be created and filled
         * with the standard JXA environment files .The map key with the name
         * System.property("os.name")); <br>
         * a LinkedHashMap is the preferred map type (ordered and unique id).
         * @param local use URL (false) or File (true) to find jar files
         * (relative to the current directory)
         * @param withInstaller adds the
         * {@link #rb ResourceBundle.extInstallerJar path-value}
         * @return 
         * @throws java.net.MalformedURLException
         * @throws java.net.URISyntaxException
         */
        public static Map<String, Map<String, URL>> _getJXAExtenvFiles(Map<String, Map<String, URL>> map, boolean local, boolean withInstaller) throws MalformedURLException, URISyntaxException {
                String jars = rb.getString("extJars");
                String jarsPathStr = rb.getString("extPathURL");
                if (jarsPathStr.endsWith("/")) {
                        jarsPathStr += "/";
                }
                File appRoot = new File(System.getProperty("jxa.appRoot", "."));
                URL jarsPath = local ? new File(appRoot, "api" + File.separator).toURI().toURL() : new URL(jarsPathStr);
                /**
                 * rebase .jar all in the same directory (appBundler 1.0)
                 */
                if (Boolean.getBoolean("jxa.appBundle")) {
                        jarsPath = appRoot.toURI().toURL();
                        if (DebugMap._getInstance().isJXADebugSysEnabled()) {
                                UIMessage.showLightPopupMessage(new JLabel("appbundle " + jarsPath), null, null, UIMessage.UI_BOTTOM_LEFT);
                        }
                }
                String jarInstaller = rb.getString("extInstallerJar");
                map = map instanceof Map ? map : Collections.synchronizedMap(new LinkedHashMap<String, Map<String, URL>>());
                Map<String, URL> exts = map.get(_getSysValue("os.name"));
                if (exts == null) {
                        map.put(_getSysValue("os.name"), exts = new LinkedHashMap<String, URL>());
                }
                for (String jar : jars.split("\\s")) {
                        exts.putAll(_getJXAExtFile(local ? jar.replaceAll("%20", " ") : jar, jarsPath));
                }
                if (withInstaller) {
                        exts.putAll(_getJXAExtFile(local ? jarInstaller.replaceAll("%20", " ") : jarInstaller, jarsPath));
                }
                if (_debugSys) {
                        if (local) {
                                System.out.println("ExtensionsInstaller (api) :: current running directory is : " + new File(".").getAbsolutePath());
                        } else {
                                System.out.println("ExtensionsInstaller (api) :: current codebase is : " + ExtensionsInstaller.class.getProtectionDomain().getCodeSource().getLocation());
                        }
                }
                return map;
        }

        /**
         * "move" URLS to another base; used to get the local version of a
         * remote (native library) file connection
         *
         * @param fullPath if true, folder path will be used as is, if false,
         * the file names only will be kept
         */
        private static <U> Map<String, Map<String, U>> __baseFileMapNat(Map<String, Map<String, U>> map, URL base, boolean fullPath) throws MalformedURLException {
                Map<String, Map<String, URL>> baseFileMap = Collections.synchronizedMap(new LinkedHashMap<String, Map<String, URL>>());
                synchronized (map) {
                        for (String os : map.keySet()) {
                                LinkedHashMap<String, URL> osMap = new LinkedHashMap<String, URL>();
                                for (Map.Entry<String, U> file : map.get(os).entrySet()) {
                                        osMap.put(file.getKey(), ((Map<URL, Boolean>) file.getValue()).keySet().iterator().next());
                                }
                                baseFileMap.put(os, osMap);
                        }
                }
                baseFileMap = __baseFileMapExt(baseFileMap, base, fullPath);
                Map<String, Map<String, U>> baseFileMapNat = new LinkedHashMap<String, Map<String, U>>();
                synchronized (baseFileMap) {
                        for (String os : baseFileMap.keySet()) {
                                LinkedHashMap<String, U> osMap = new LinkedHashMap<String, U>();
                                for (Map.Entry<String, URL> file : baseFileMap.get(os).entrySet()) {
                                        osMap.put(file.getKey(), (U) Collections.singletonMap(file.getValue(), ((Map<URL, Boolean>) map.get(os).get(file.getKey())).values().iterator().next()));
                                }
                                baseFileMapNat.put(os, osMap);
                        }
                }
                return Collections.synchronizedMap(baseFileMapNat);
        }

        /**
         * "move" URLS to another base; used to get the local version of a
         * remote file connection
         *
         * @param fullPath if true, folder path (which is right after
         * server-hostname/) will be used as is, if false, the file names only
         * will be kept
         */
        private static <U> Map<String, Map<String, U>> __baseFileMapExt(Map<String, Map<String, U>> map, URL base, boolean fullPath) throws MalformedURLException {
                Map<String, Map<String, U>> baseFileMap = Collections.synchronizedMap(new LinkedHashMap<String, Map<String, U>>());
                synchronized (map) {
                        for (Map.Entry<String, Map<String, U>> osMap : map.entrySet()) {
                                Map<String, URL> osfileMap = (Map<String, URL>) osMap.getValue();
                                Map<String, U> osbasefileMap = new LinkedHashMap<String, U>();
                                for (String id : osfileMap.keySet()) {
                                        URL path = osfileMap.get(id);
                                        if (path == null) {
                                                osbasefileMap.put(id, null);
                                        } else if (base == null) {
                                                osbasefileMap.put(id, (U) path);
                                        } else {
                                                String sPath = fullPath ? path.getPath() : new File(path.getPath()).getName();
                                                while (sPath.startsWith("/") && sPath.length() > 1) {
                                                        sPath = sPath.substring(1);
                                                }
                                                osbasefileMap.put(id, (U) new URL(base, sPath));
                                        }
                                }
                                baseFileMap.put(osMap.getKey(), osbasefileMap);
                        }
                }
                return baseFileMap;
        }

        private static Map<String, Map<String, URL>> __urlFileMap(Map<String, Map<String, File>> map) throws MalformedURLException {
                Map<String, Map<String, URL>> urlMap = new LinkedHashMap<String, Map<String, URL>>();
                synchronized (map) {
                        for (String os : map.keySet()) {
                                Map<String, URL> osfileMap = new LinkedHashMap<String, URL>();
                                for (String id : map.get(os).keySet()) {
                                        if (map.get(os).get(id) == null) {
                                                osfileMap.put(id, null);
                                        } else {
                                                osfileMap.put(id, map.get(os).get(id).toURI().toURL());
                                        }
                                }
                                urlMap.put(os, osfileMap);
                        }
                }
                return Collections.synchronizedMap(urlMap);
        }

        /**
         * @param map a map to add the files or null to create a new map
         * @param withInstaller adds the ResourceBundle.extInstallerJar
         * path-value
         * @param base a base URL that will be used as the base path for env
         * files in the returned map
         * @param fullPath if true, folder path will be used as is and appended
         * to base; if false, the file names (in map) only will be kept
         * @return 
         * @throws java.net.MalformedURLException
         * @throws java.net.URISyntaxException
         */
        public static Map<String, Map<String, URL>> _getJXAExtenvFilesFromBase(Map<String, Map<String, URL>> map, URL base, boolean withInstaller, boolean fullPath) throws MalformedURLException, URISyntaxException {
                return __baseFileMapExt(_getJXAExtenvFiles(map, true, withInstaller), base, fullPath);
        }


        /**
         * adds the
         * {@link #_getJXAExtenvFilesFromBase(java.util.Map, java.net.URL, boolean, boolean) jxa extension files}
         * to the urls array. Environment settings are kept from the specified
         * JXAenvUtils instance.
         * @param env
         * @return 
         */
        public static URL[] _getURLClassLoaderURLS(JXAenvUtils env) {
                /*if (extDir == null) {
         extDir = new File(_findExtPath(false));
         }*/
                /**
                 * add extensions jar files
                 */
                try {
                        Map<String, URL> jarMap = _getJXAExtenvFiles(env.envJars, env.isLocal(), false).get(_getSysValue("os.name"));
                        /**
                         * for classloading
                         */
                        List<URL> classloaderJars = new ArrayList<URL>();
                        /**
                         * recover previous jars
                         */
                        synchronized (jarMap) {
                                for (URL jarUrl : jarMap.values()) {
                                        try {
                                                if (env.keepReadingOnRemoteJarResources) {
                                                        classloaderJars.add(jarUrl);
                                                } else {
                                                        classloaderJars.add(env.findEnvInstalledFile(jarUrl).toURI().toURL());
                                                }
                                        } catch (MalformedURLException ex) {
                                                ex.printStackTrace();
                                        }
                                }
                        }
                        return classloaderJars.toArray(new URL[]{});
                } catch (Exception ex) {
                        if (_debugSys) {
                                ex.printStackTrace();
                        }
                        return null;
                }
        }

        /**
         * look for any "Class-Path:" manifest .jar-entry (folder-classpaths are
         * ignored)
         * @param jarUrl
         * @return 
         * @throws java.io.IOException
         */
        public static Map<String, URL> _getJarClasspathJars(URL jarUrl) throws IOException {
                URL u = new URL("jar:" + jarUrl + "!/");
                Map<String, URL> jarUrls = new LinkedHashMap<String, URL>();
                URLConnection jar = u.openConnection();
                if (jar instanceof JarURLConnection) {
                        /**
                         *
                         */
                        Manifest m = ((JarURLConnection) jar).getManifest();
                        if (m != null) {
                                Attributes a = m.getMainAttributes();
                                if (a != null) {
                                        String classpath = a.getValue(Attributes.Name.CLASS_PATH);
                                        if (classpath != null) {
                                                for (String path : classpath.split("\\s")) {
                                                        if (path != null && path.endsWith(".jar")) {
                                                                URL nJarUrl = new URL(jarUrl.toString().substring(0, jarUrl.toString().lastIndexOf("/")) + "/" + path);
                                                                String name = FileHelper._getURLFilename(u);
                                                                jarUrls.put(name, nJarUrl);
                                                        }
                                                }
                                        }
                                }
                        }
                }
                return jarUrls;
        }
}
