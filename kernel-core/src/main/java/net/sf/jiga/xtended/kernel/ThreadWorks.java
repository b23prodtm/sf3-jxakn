/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sf.jiga.xtended.kernel;

import java.awt.event.ActionEvent;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.security.AccessControlContext;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import net.sf.jiga.xtended.JXAException;
import net.sf.jiga.xtended.ui.UIMessage;

/**
 * ThreadWorks gives the developer to "schedule" tasks (or "works") for the the
 * same Thread is going to process them, within a single ThreadWorks instance.
 * E.g. JXA GameActionLayer use a static ThreadWorks instance to load resources
 * over the time, one each after the other. It's safer to invoke long tasks with
 * such a ThreadWorks (as the SwingWorker Swing class does, too) because if the
 * task is cancelled, the application can recover quickly for the task is run
 * over a dedicated Thread of the {@linkplain #workTG works ThreadGroup}.<br>
 * However, it shouldn't be used for tasks that may require a quick startup,
 * because each task instances a new Thread (if
 * {@linkplain #isMultiThreadingEnabled()} joining the
 * {@linkplain #workDT global dispatch Thread}.<br>
 * <i>Implementation notice : The implementation of this class with the
 * java.util.Timer is slightly different from the usual schedules of Timer :
 * scheduled tasks are enqueued in a local Stack to guarranty a (FIFO)
 * first-in-first-out execution, among all methods calls ({@link #doAndWait(java.lang.Runnable)
 * }, {@link #doLater(java.lang.Runnable) },...), e.g. doLater(Task0);
 * doAndWait(Task1); => code will wait that Task0 is over before to run and wait
 * for Task1. </i>
 *
 * @see #securizedInvokeLater(java.lang.Runnable)
 * @author www.b23prodtm.info
 */
public class ThreadWorks implements Threaded {

    /**
     *
     * @param r the value of r
     */
    private static <T> Callable<T> loaderRun(final Callable<T> r) {
        return new Callable<T>() {
            public T call() {
                T ret = null;
                try {
                    ret = r.call();
                } catch (ExecutionException ex) {
                    throw ex;
                } finally {
                    return ret;
                }
            }
        };
    }

    public boolean isMultiThreadingEnabled() {
        return JXAenvUtils._multiThreading;
    }

    public void setMultiThreadingEnabled(boolean b) {
        JXAenvUtils._multiThreading = b;
    }

    public Monitor[] getGroupMonitor() {
        return new Monitor[]{workMon};
    }

    public void setGroupMonitor(Monitor... tg) {
        workMon = tg[0];
    }

    /**
     * for use with
     * {@linkplain ThreadWorks#doAndWaitReturn(net.sf.jiga.xtended.kernel.ThreadWorks, net.sf.jiga.xtended.kernel.ThreadWorks.DoStaticReturn)}
     *
     * @param <T>
     * @deprecated
     */
    public static abstract class DoStaticReturn<T> implements Callable<T> {

        /**
         * must return a {@linkplain <T>} value or null
         *
         * @return
         * @throws Throwable
         */
        public abstract T run() throws Throwable;

            /**
             *
             * @return
             * @throws Exception
             */
            public final T call() throws Exception {
            try {
                return run();
            } catch (Throwable ex) {
                Logger.getLogger(JXAenvUtils.LOGGER_NAME).log(Level.SEVERE, null, ex);
                return null;
            }
        }

    }
    private final static AccessControlContext _acc = AccessController.getContext();

    private static ThreadWorks _loadSecurityThreadWorks() {
        return AccessController.doPrivileged(new PrivilegedAction<ThreadWorks>() {
            public ThreadWorks run() {
                return new ThreadWorks("JXA_Security", true);
            }
        }, _acc);
    }
    /**
     * This the security ThreadWorks, used by some classes to commit tasks
     */
    public final static ThreadWorks securityTWKS = _loadSecurityThreadWorks();

    /**
     * returns true or false, whether the current Thread owner is the securized
     * Dispatcher Thread (SDT)
     *
     * @return true or false, whether the current Thread owner is the securized
     * Dispatcher Thread (SDT)
     * @see #securizedInvokeAndWait(Runnable)
     * @see #securizedInvokeLater(Runnable)
     */
    public final static boolean isSDT() {
        return securityTWKS.isDT();
    }

    /**
     * invokes a Runnable onto a new separate Thread to securize the running
     * Thread against caught exceptions. This is very useful for not getting the
     * EDT-cycle closed with Swing applications. Unlike with the Swing-EDT
     * methods ({@link SwingUtilities}), all uncaught exceptions appear onto a
     * separate exception handler (static CoalescedThreadsMonitor), it is
     * scheduled on the security timer and the current Thread waits for its
     * termination ({@link Thread#join() join()}) NOTICE : as with the
     * SwingUtilities, this method cannot be called from the
     * {@linkplain #isSDT() SDT} ThreadGroup
     *
     * @param r the Runnable instance to run() in securized environment.
     * @throws InterruptedException
         * @throws java.util.concurrent.TimeoutException
         * @throws java.util.concurrent.ExecutionException
     */
    public final static void securizedInvokeAndWait(Runnable r) throws InterruptedException, TimeoutException, ExecutionException {
        securityTWKS.doAndWait(r);
    }

    /**
     * invokes a Runnable onto a new separate Thread to securize the running
     * Thread against caught exceptions. This is very useful for not getting the
     * EDT-cycle closed with Swing applications. Unlike with the Swing-EDT
     * methods ({@link SwingUtilities}), all uncaught exceptions appear onto a
     * separate exception handler (static CoalescedThreadsMonitor) and it is
     * scheduled on the security timer.
     *
     *
     * @param r the Runnable instance to run() in securized environment.
     */
    public final static void securizedInvokeLater(Runnable r) {
        securityTWKS.doLater(r);
    }

    /**
     * dynamically compares and, if needed, changes 2 ThreadGroup's priorities
     * so that the first "up" has a higher priority than the second "down".
     * Either the first one or the second one is "pushed up" or "pulled down",
     * resp.
     *
     * @param up the ThreadGroup to "push up"
     * @param down the ThreadGroup to "pull down"
     */
    public final static void prioritizeThreadGroups(ThreadGroup up, ThreadGroup down) {
        if (up.getMaxPriority() > Thread.MIN_PRIORITY) {
            down.setMaxPriority(Math.min(down.getMaxPriority(), up.getMaxPriority() - 1));
            if (JXAenvUtils._debugSys) {
                System.out.println(down.toString() + " TG is pulled down to pty : " + down.getMaxPriority());
            }
        } else {
            up.setMaxPriority(Math.max(up.getMaxPriority(), down.getMaxPriority() + 1));
            if (JXAenvUtils._debugSys) {
                System.out.println(up.toString() + " TG is pushed up to pty : " + up.getMaxPriority());
            }
        }
    }

    private ScheduledThreadPoolExecutor newDispatcher(final String name) {
        int concurrentThreads = JXAenvUtils._multiThreading ? Runtime.getRuntime().availableProcessors() : 1;
        return new ScheduledThreadPoolExecutor(concurrentThreads, new ThreadFactory() {

            public Thread newThread(Runnable r) {
                Thread t = new Thread(workTG, r, "TWKS-" + name + "-DT");
                t.setDaemon(isDaemon);
                return t;
            }
        });
    }

    /**
     * delay between each scheduled task
     */
    long delay = 10;
    TimeUnit timeunit = TimeUnit.MILLISECONDS;

    /**
         * @param delay
         * @param timeunit
     * @see
     * ScheduledThreadPoolExecutor#scheduleWithFixedDelay(java.lang.Runnable,
     * long, long, java.util.concurrent.TimeUnit) java.util.concurrent.TimeUnit)
     */
    protected void setDelay(long delay, TimeUnit timeunit) {
        this.delay = delay;
        this.timeunit = timeunit;
    }

        /**
         *
         * @return
         */
        protected long getDelay() {
        return delay;
    }

        /**
         *
         * @return
         */
        protected TimeUnit getTimeunit() {
        return timeunit;
    }

    boolean isDaemon = false;

    /**
     * {@link #cancelAllSchedules() } if state is changed.
     *
         * @param isDaemon
     * @default value is false
     */
    public void setIsDaemon(boolean isDaemon) {
        boolean b = this.isDaemon;
        this.isDaemon = isDaemon;
        if (b != isDaemon) {
            cancelAllSchedules();
        }
    }

    String name;

    /**
     * namely the ThreadWorks instance is created and it can be monitored
     * through the {@linkplain #workTG ThreadGroup} and the
     * {@linkplain #workDT dispatcher}
     *
     * @param name
     */
    public ThreadWorks(String name) {
        this(name, false, null);
    }

    /**
     * namely the ThreadWorks instance is created and it can be monitored
     * through the {@linkplain #workTG ThreadGroup} and the
     * {@linkplain #workDT dispatcher}
     *
     * @param name
     * @param monitor synch monitor
     */
    public ThreadWorks(String name, Monitor monitor) {
        this(name, false, monitor);
    }

    boolean highestPriority;

    private ThreadWorks(String name, boolean highestPriority, Monitor monitor) {
        workTG = new ThreadGroup("TWKS-" + name + "-TG");
        workMon = monitor;
        this.highestPriority = highestPriority;
        initThreadWorks(name);
    }

    private ThreadWorks(String name, boolean highestPriority) {
        workTG = new ThreadGroup("TWKS-" + name + "-TG");
        workMon = new Monitor();
        this.highestPriority = highestPriority;
        initThreadWorks(name);
    }

    /**
     * if you use {@link #workDT} to invoke tasks, then checkAlive checks that a
     * Timer is alive for dispatching tasks.
     *
     * @return if false, all scheduled tasks must be reloaded again
     */
    public boolean checkAlive() {
        try {
            workDT.schedule(new Runnable() {
                @Override
                public void run() {
                    /**
                     * schedule is tested
                     */
                }
            }, 0, TimeUnit.MILLISECONDS);
            return true;
        } catch (RejectedExecutionException ex) {
            int n = workDT.shutdownNow().size();
            System.err.println(JXAenvUtils.log(n + " tasks have been purged because the timer was dead. (TWKS " + name + ")", JXAenvUtils.LVL.SYS_NOT));
            if (DebugMap._getInstance().isDebugLevelEnabled(DebugMap._getInstance()._VOID)) {
                ex.printStackTrace();
            }
            initThreadWorks(name);
            return false;
        }
    }

    private void initThreadWorks(String name) {
        workDT = newDispatcher(name);
        this.name = name;
        if (!highestPriority) {
            prioritizeThreadGroups(securityTWKS.workTG, workTG);
        }
    }

    /**
     * reports every uncaught Exception of this ThreadGroup. An UIMessage pop-up
     * is shown.
     *
     * @param throwable the Throwable Exception that has not been caught
     */
    public static void cLogFilePrintStackStrace(Throwable throwable) {
        throwable.printStackTrace(cLogFilePrintStream);
    }
    /**
     * the current UI message sent to the user
     */
    private static UIMessage currentMessage = null;
    /**
     * the previous UI message sent to the user
     */
    private static String previousMessage = "";

        /**
         *
         * @param modal
         * @param thread
         * @param throwable
         */
        public static void _uncaughtException(final boolean modal, final Thread thread, final Throwable throwable) {
        cLogFilePrintStackStrace(throwable);
        Runnable r = new Runnable() {
            public void run() {
                __uncaughtException(modal, thread, throwable);
            }
        };
        if (ThreadWorks.Swing.isEventDispatchThread()) {
            r.run();
        } else {
            /**
             * void awtthreadlock blocking
             */
            ThreadWorks.Swing.invokeLater(r);
        }
    }

    private static void __uncaughtException(boolean modal, Thread thread, Throwable throwable) {
        throwable.printStackTrace();
        if (currentMessage instanceof UIMessage) {
            if (currentMessage.isShowing()) {
                currentMessage.dispose();
                previousMessage += "\r\n\r\n";
            } else {
                previousMessage = "";
            }
        } else {
            previousMessage = "";
        }
        String stack = "";
        for (StackTraceElement ste : throwable.getStackTrace()) {
            stack += "\n" + ste;
        }
        String stack_c = "";
        if (throwable.getCause() instanceof Throwable) {
            for (StackTraceElement ste : throwable.getCause().getStackTrace()) {
                stack_c += "\n" + ste;
            }
        }
        String msg = "an unexpected exception has been caught : " + throwable.getClass().getName() + " : " + throwable.getMessage() + " \n\r" + stack + " \n in Thread " + thread.getName() + ((throwable.getCause() instanceof Throwable) ? (" caused by : " + throwable.getCause().getClass().getName() + " : " + throwable.getCause().getMessage() + "\n\r" + stack_c) : "");
        currentMessage = new UIMessage(
                modal,
                previousMessage += msg,
                null,
                UIMessage.ERROR_TYPE);
        currentMessage.setSize(500, 150);
        currentMessage.repaint();
    }
    /**
     *
     */
    public static Console c = new Console();
    /**
     * the log file used to store uncaught exceptions (from {@link #c Console})
     */
    protected static File cLogFile = null;

    /**
     * closes the log file used to store uncaught exceptions (from
     * {@link #c Console})
         * @param name
     */
    protected static void cLogFile_open(String name) {
        FileOutputStream logOutput_fos = null;
        cLogFile = new File(FileHelper._USERHOMESTOREDIRECTORY, "log_" + name + ".txt");
        try {
            FileHelper._makeWritable(cLogFile);
            logOutput_fos = new FileOutputStream(cLogFile, true);
            logOutput_fos.getChannel().truncate(102400L);
            PrintWriter logOutput = new PrintWriter(new BufferedOutputStream(logOutput_fos));
            c.setOutput(logOutput);
            logOutput.println();
            logOutput.println("Log started on " + new Date(System.currentTimeMillis()).toString());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * closes the log file used to store uncaught exceptions (from
     * {@link #c Console}) JXAenvUtils closes the file in a Threadhook before
     * the VM exits.
     */
    protected static void cLogFile_close() {
        Appendable p = c.setOutput(null);
        if (p instanceof Closeable) {
            try {
                ((Closeable) p).close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
    /**
     * the uncaught exceptions output stream can be changed to any valid
     * PrintStream
     */
    public static PrintStream cLogFilePrintStream = c.getNewPrintStream(new ErrorListener() {
        public void printStreamError(IOException e) {
            System.err.println(JXAenvUtils.log(cLogFile.getAbsolutePath() + " Kernel log file error : Printstream lost. " + e.getMessage() + " " + e.getCause(), JXAenvUtils.LVL.SYS_WRN));
        }
    });

    /**
     * reports every uncaught Exception of this ThreadGroup. An UIMessage pop-up
     * is shown.
     *
     * @param thread the Thread that hasn't caught the Exception
     * @param throwable the Throwable Exception that has not been caught
     */
    public void uncaughtException(final Thread thread, final Throwable throwable) {
        _uncaughtException(true, thread, throwable);
    }
    /**
     * This groups the Threads run with.
     *
     * @see #doAndWait(Runnable)
     * @see #doLater(Runnable)
     */
    public final ThreadGroup workTG;
    private Monitor workMon;
    /**
     * this is the work Timer, a.k.a. the Work Dispatcher Thread.
     */
    public ScheduledThreadPoolExecutor workDT;

    /**
     * returns true or false, whether the current Thread owner is the works
     * Dispatcher Thread (DT)
     *
     * @return true or false, whether the current Thread owner is the works
     * Dispatcher Thread (DT)
     */
    public boolean isDT() {
        return Thread.currentThread().getThreadGroup().equals(workTG);
    }

    /**
     *
     * @param r
     * @throws InterruptedException
         * @throws java.util.concurrent.TimeoutException
         * @throws java.util.concurrent.ExecutionException
     */
    public void doAndWait(final Runnable r) throws InterruptedException, TimeoutException, ExecutionException {
        doAndWait(r, null);
    }

    /**
     *
     * @param <T>
     * @param twks
     * @param r
     * @return
     * @throws InterruptedException
     * @throws InvocationTargetException
         * @throws java.util.concurrent.ExecutionException
         * @throws java.util.concurrent.TimeoutException
     */
    public static <T> T doAndWaitReturn(ThreadWorks twks, final Callable<T> r) throws InterruptedException, InvocationTargetException, ExecutionException, TimeoutException {
        return doAndWaitReturn(twks, r, null);
    }

    /**
     * invokes a Runnable onto a new separate Thread to process the running
     * Thread against caught exceptions. This is very useful for not getting the
     * EDT-cycle closed with Swing applications. Unlike with the Swing-EDT
     * methods ({@link SwingUtilities}), all uncaught exceptions appear onto a
     * separate exception handler (static CoalescedThreadsMonitor), it is
     * scheduled on the work timer and the current Thread waits for its
     * termination ({@link Thread#join() join()}) NOTICE : as with the
     * SwingUtilities, this method cannot be called from the
     * {@linkplain #isSDT() SDT} ThreadGroup
     *
     * @param r the Runnable instance to run() in the working environment.
     * @param cacheHandler if the operation is memorySensitive, then a
     * cacheHandler should be specified, with which a memorySensitiveCallback
     * will be attempted.
     * @throws InterruptedException
         * @throws java.util.concurrent.TimeoutException
         * @throws java.util.concurrent.ExecutionException
     */
    public void doAndWait(final Runnable r, final SpritesCacheManager cacheHandler) throws InterruptedException, TimeoutException, ExecutionException {
        doAndWait(r, cacheHandler, _DEFAULT_TIMEOUT, _DEFAULT_TIMEOUT_UNIT);
    }

        /**
         *
         * @param r
         * @param cacheHandler
         * @param timeout
         * @param timeoutunit
         * @throws InterruptedException
         * @throws TimeoutException
         * @throws ExecutionException
         */
        public void doAndWait(final Runnable r, final SpritesCacheManager cacheHandler, long timeout, TimeUnit timeoutunit) throws InterruptedException, TimeoutException, ExecutionException {
        doAndWaitReturn(this, new Callable<Runnable>() {
            public Runnable call() {
                r.run();
                return r;
            }
        }, cacheHandler, timeout, timeoutunit);
    }

    /**
     *
     * @param <T> class that is returned by the DoStaticReturn T 
     * @param twks ThreadWorks that runs the block of instructions
     * @param r the block of instructions to run
     * @param cacheHandler if the operation is memorySensitive, then a
     * cacheHandler should be specified, with which a memorySensitiveCallback
     * will be attempted.
     * @return T object is returned
     * @throws java.util.concurrent.ExecutionException callable failed
     * @throws java.util.concurrent.TimeoutException callable took more time
     * than timeout
     * @throws InterruptedException
     */
    public static <T> T doAndWaitReturn(final ThreadWorks twks, final Callable<T> r, final SpritesCacheManager cacheHandler) throws ExecutionException, TimeoutException, InterruptedException {
        return doAndWaitReturn(twks, r, cacheHandler, _DEFAULT_TIMEOUT, _DEFAULT_TIMEOUT_UNIT);
    }

        /**
         *
         */
        public static long _DEFAULT_TIMEOUT = 100;

        /**
         *
         */
        public static TimeUnit _DEFAULT_TIMEOUT_UNIT = TimeUnit.SECONDS;

    /**
         * @param <T>
         * @param twks
     * @param timeout reasonable timeout to wait for returning callable
         * @param r
         * @param cacheHandler
         * @param timeoutunit
         * @return 
         * @throws java.util.concurrent.ExecutionException
         * @throws java.util.concurrent.TimeoutException
         * @throws java.lang.InterruptedException
     */
    public static <T> T doAndWaitReturn(final ThreadWorks twks, final Callable<T> r, final SpritesCacheManager cacheHandler, long timeout, TimeUnit timeoutunit) throws ExecutionException, TimeoutException, InterruptedException {
        twks.checkAlive();
        if (twks.isDT()) {
            throw new JXAException("can't call doAndWait on the work Dispatcher Thread (DT).");
        }
        final Callable<T> rCache = loaderRun(new Callable<T>() {
            public T call() throws Exception {
                if (cacheHandler != null) {
                    try {
                        return (T) cacheHandler.memorySensitiveCallback("call", r, new Object[]{}, new Class[]{});
                    } catch (Throwable ex) {
                        throw new Exception(ex);
                    }
                } else {
                    return r.call();
                }
            }
        });
        ScheduledFuture<T> s = twks.workDT.schedule(rCache, 0, twks.timeunit);
        return s.get(timeout, timeoutunit);
    }

    /**
     *
     * @param r
     */
    public void doLater(final Runnable r) {
        doLater(r, null);
    }

    /**
     * invokes a Runnable onto a new separate Thread to process the running
     * Thread against caught exceptions. Unlike with the Swing-EDT methods
     * ({@link SwingUtilities}), all uncaught exceptions appear onto a separate
     * exception handler (static CoalescedThreadsMonitor) and it is scheduled on
     * the work timer.
     *
     *
     * @param r the Runnable instance to run() in th working environment.
     ** @param cacheHandler if the operation is memorySensitive, then a
     * cacheHandler should be specified, with which a memorySensitiveCallback
     * will be attempted.
     * @see #_BOTTOM_RIGHT
     * @see #_TOP_RIGHT
     * @see #_TOP_LEFT
     * @see #_BOTTOM_LEFT
     */
    public void doLater(final Runnable r, final SpritesCacheManager cacheHandler) {
        doLaterReturn(new Callable() {

            public Object call() throws Exception {
                r.run();
                return null;
            }
        }, cacheHandler);
    }

        /**
         *
         * @param <T>
         * @param r
         * @param cacheHandler
         * @return
         */
        public <T> ScheduledFuture<T> doLaterReturn(final Callable<T> r, final SpritesCacheManager cacheHandler) {
        checkAlive();
        final Callable<T> rCache = loaderRun(new Callable<T>() {

            public T call() throws Exception {
                if (cacheHandler != null) {
                    try {
                        return (T) cacheHandler.memorySensitiveCallback("call", r, new Object[]{}, new Class[]{});
                    } catch (Throwable ex) {
                        ex.printStackTrace();
                    }
                } else {
                    return r.call();
                }
                return null;
            }
        });
        return workDT.schedule(rCache, delay, timeunit);
    }

    /**
     * Terminates by interrrupting all scheduled tasks and restarts a
     * {@link ScheduledThreadPoolExecutor new dispatcher Thread}.
     *
     * @see ScheduledThreadPoolExecutor#shutdownNow()
     */
    public void cancelAllSchedules() {
        ScheduledThreadPoolExecutor workDT = this.workDT;
        this.workDT = newDispatcher(name);
        workDT.shutdownNow();
    }

    /**
     * Swing Thread Wrapper. This performs a context ClassLoader switch before
     * each call to the Swing EDT.
     */
    public static class Swing {

        /**
         * convenience method. It simply calls
         * SwingUtilities.isEventDispatchThread().
         *
         * @return
         */
        public static boolean isEventDispatchThread() {
            return SwingUtilities.isEventDispatchThread();
        }

        /**
         * WRAPPER performs context switch
         *
         * @param r
         */
        public static void invokeLater(Runnable r) {
            SwingUtilities.invokeLater(r);
        }

        /**
         * WRAPPER performs context switch
         *
         * @param r
         * @throws InterruptedException
         * @throws InvocationTargetException
         */
        public static void invokeAndWait(Runnable r) throws InterruptedException, InvocationTargetException {
            if (isEventDispatchThread()) {
                throw new JXAException("Illegal call to invokeAndWait while being on the Event Dispatcher Thread.");
            }
            SwingUtilities.invokeAndWait(r);
        }

        /**
         * allows a Swing context to be run with a returned value. it checks if
         * the {@linkplain ThreadWorks.Swing#invokeAndWait(Runnable)} can be run
         * (if not, it will run on the current EDT anyway) and returns the value
         * after executing the {@linkplain SwingStaticReturn#run()} method
             * @param <T>
             * @param r
             * @return 
             * @throws java.lang.InterruptedException 
             * @throws java.lang.reflect.InvocationTargetException
         */
        public static <T> T invokeSwingAndReturnException(final SwingStaticReturnException<T> r) throws InterruptedException, InvocationTargetException {
            final Map<String, Object> ret = new HashMap<String, Object>();
            ret.put("return", null);
            ret.put("throw", null);
            Runnable rSw = new Runnable() {
                public void run() {
                    try {
                        ret.put("return", r.run());
                    } catch (Throwable ex1) {
                        ret.put("throw", ex1);
                    }
                }
            };
            if (ThreadWorks.Swing.isEventDispatchThread()) {
                try {
                    return r.run();
                } catch (Throwable ex) {
                    throw new InvocationTargetException(ex);
                }
            } else {
                ThreadWorks.Swing.invokeAndWait(rSw);
                if (ret.get("throw") instanceof Throwable) {
                    throw new InvocationTargetException((Throwable) ret.get("throw"));
                }
                return (T) ret.get("return");
            }
        }

        /**
         * allows a Swing context to be run with a returned value. it checks if
         * the {@linkplain SwingUtilities#invokeAndWait(Runnable)} can be run
         * (if not, it will run on the current EDT anyway) and returns the value
         * after executing the {@linkplain SwingStaticReturn#run()} method
             * @param <T>
             * @param r
             * @return 
             * @throws java.lang.InterruptedException 
             * @throws java.lang.reflect.InvocationTargetException 
         */
        public static <T> T invokeSwingAndReturn(final SwingStaticReturn<T> r) throws InterruptedException, InvocationTargetException {
            final Map<String, T> ret = new HashMap<String, T>((Map<String, T>) Collections.singletonMap("return", null));
            Runnable rSw = new Runnable() {
                public void run() {
                    ret.put("return", r.run());
                }
            };
            if (ThreadWorks.Swing.isEventDispatchThread()) {
                return r.run();
            } else {
                ThreadWorks.Swing.invokeAndWait(rSw);
                return ret.get("return");
            }
        }
    }

    /**
     * for use with
     * {@linkplain UIMessage#invokeSwingAndReturn(net.sf.jiga.xtended.ui.UIMessage.SwingStaticReturn)}
         * @param <T>
     */
    public static interface SwingStaticReturn<T> {

        /**
         * must return a {@linkplain <T>} value or null
             * @return 
         */
        public T run();
    }

    /**
     * for use with
     * {@linkplain UIMessage#invokeSwingAndReturnException(net.sf.jiga.xtended.ui.UIMessage.SwingStaticReturnException)}
         * @param <T>
     */
    public static interface SwingStaticReturnException<T> {

        /**
         * must return a {@linkplain <T>} value or null
             * @return 
             * @throws java.lang.Throwable
         */
        public T run() throws Throwable;
    }

        /**
         * defines a lengthy operation
         */
        public static class Lengthy {

            /**
             * creates an instance
             */
            public Lengthy() {
        }

        /**
         *
         */
        private long MILLIS_TO_SHOW_LENGTHY_OPERATION_POPUP = 5000;

        /**
         * milliseconds before to show a popup cancel for a lengthy operation
         * (if in a
         * {@link JXAenvUtils#_multiThreading multithreaded environment only})
         * set a negative value to never show this popup; set zero to always
         * show when a ThreadWorks is in run
         *
         * @param millis set a negative value to never show this popup; set zero
         * to always show when a ThreadWorks is in run
         */
        public void setMillisToShowLengthyOperationPopup(long millis) {
            MILLIS_TO_SHOW_LENGTHY_OPERATION_POPUP = millis;
        }

        /**
         * @return @see #setMillisToShowLengthyOperationPopup(long)
         * @default 5000 ms
         */
        public long getMillisToShowLengthyOperationPopup() {
            return MILLIS_TO_SHOW_LENGTHY_OPERATION_POPUP;
        }

            /**
             *
             * @param t
             * @param parent
             * @param popupLocation
             * @return
             */
            public JDialog showPopupLengthyOperation(final Thread t, final JComponent parent, int popupLocation) {
            return UIMessage.showLightPopupMessage(new JLabel("Running Task..."), new AbstractAction("Abort", UIMessage._getIcon(UIMessage.ABORT_TYPE, true)) {
                public void actionPerformed(ActionEvent e) {
                    t.interrupt();
                }
            }, parent, popupLocation);
        }
    }
}
