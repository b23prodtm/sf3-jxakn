/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sf.jiga.xtended.kernel;

import net.sf.jiga.xtended.kernel.ThreadBlock;

/**
 * To handle a block in a Thread where some local parameter may be used.
 *
 * @author www.b2prodtm.info
 * @param <D>
 */
public class ThreadBlockData<D> extends ThreadBlock {

        private ThreadLocal<D> current;
        private ThreadLocal<D> temp;

        /**
         * delegates a ThreadLocal instance with the initial data (may be null)
         *
         * @param data the {@link ThreadLocal#initialValue() } to initialize.
         */
        public ThreadBlockData(final D data) {
                super();
                current = new ThreadLocal<D>() {

                        @Override
                        protected D initialValue() {
                                return data == null ? super.initialValue() : data;
                        }
                };
                temp = new ThreadLocal<D>();
        }

        /**
         * creates an instance without data
         */
        public ThreadBlockData() {
                this(null);
        }

        /**
         *
         * @return
         */
        public D get() {
                return current.get();
        }

        /**
         *
         * @param tdata
         */
        public void set(D tdata) {
                this.current.set(tdata);
        }

        /**
         * removes the data from current
         */
        public void remove() {
                this.current.remove();
        }

        /**
         *
         * @param data
         */
        public void begin(D data) {
                super.begin();
                temp.set(get());
                set(data);
        }

        @Override
        public void end() {
                super.end();
                if (temp.get() != null) {
                        set(temp.get());
                        temp.remove();
                } else {
                        remove();
                }
        }

}
