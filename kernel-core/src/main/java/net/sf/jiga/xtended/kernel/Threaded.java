/* * Threaded.java
 Created on 21 septembre 2007, 06:15
 To change this template, choose Tools | Template Manager * and open the template in the editor. */package net.sf.jiga.xtended.kernel;

/**
 *
 * the Threaded interface defines how to manage groups of threads as
 * ThreadGroup's
 *
 *
 * @author www.b23prodtm.info
 */
public interface Threaded {

        /**
         * returns true or false whether the Resource is runnning multi-threaded
         * or not, resp.
         *
         * @return true or false
         * @see #setMultiThreadingEnabled(boolean)
         */
        public boolean isMultiThreadingEnabled();

        /**
         * dis/enables the multi-threading mode for this Resource
         *
         * @param b dis/enables the multi-threading mode for this Resource
         */
        public void setMultiThreadingEnabled(boolean b);

        /**
         *
         * synchronized(){} Monitor instance
         * @return 
         */
        public Monitor[] getGroupMonitor();

        /**
         *
         * synchronized(){} Monitor instance
         *
         * @param tg
         */
        public void setGroupMonitor(Monitor... tg);
}
