package net.sf.jiga.xtended.ui;

import java.awt.BorderLayout;
import java.awt.Dialog;
import netscape.javascript.JSObject;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.WeakHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ListChangeListener.Change;
import javafx.concurrent.Worker;
import javafx.concurrent.Worker.State;
import javafx.embed.swing.JFXPanel;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.web.PopupFeatures;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebErrorEvent;
import javafx.scene.web.WebEvent;
import javafx.scene.web.WebHistory;
import javafx.scene.web.WebHistory.Entry;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Callback;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRootPane;
import net.sf.jiga.xtended.JXAException;
import net.sf.jiga.xtended.kernel.JXAenvUtils;
import net.sf.jiga.xtended.kernel.Monitor;
import org.w3c.dom.Document;

/**
 * JXAWebView usage : {@link #newBrowser(java.net.URL)} Then with
 * {@link Browser#webEngine} you can inject javascript snippets. <br>Always
 * encapsulate {@link WebEngine#executeScript(java.lang.String)} with
 * {@link Platform#runLater(java.lang.Runnable)}, as it will be able to debug
 * from the WebView instance. Exception are reported to the Java application.
 *
 * @author www.b23prodtm.info . Credits to
 * https://gist.github.com/anjackson/1640654
 */
public class JXAWebView extends Application {

    @Override
    public void start(final Stage stage) {
        browser = new Browser(this, homepage, null);
        Scene scene = browser.createScene();
        // show stage
        stage.setScene(scene);

        browser.webEngine.titleProperty().addListener(new ChangeListener<String>() {

            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if (stage != null) {
                    stage.setTitle(t1);
                }
            }
        });

        stage.show();
        initialized = true;
    }

        /**
         *
         */
        protected Browser browser = null;

    /**
     * Opens a browser JavaFX WebView
     *
     * @param args -homepage url -javaApp className -width w -height h
         * @throws java.lang.InterruptedException
         * @throws java.util.concurrent.ExecutionException
     */
    public static final void main(String[] args) throws InterruptedException, ExecutionException {
        boolean embed = false;
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-homepage")) {
                try {
                    homepage = new URL(args[++i]);
                } catch (MalformedURLException ex) {
                    Logger.getLogger(JXAenvUtils.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (args[i].equals("-javaApp")) {
                try {
                    javaApp = Class.forName(args[++i]).newInstance();
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(JXAenvUtils.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                    Logger.getLogger(JXAenvUtils.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(JXAenvUtils.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (args[i].equals("-width")) {
                preferredSize.width = Integer.parseInt(args[++i]);
            } else if (args[i].equals("-height")) {
                preferredSize.height = Integer.parseInt(args[++i]);
            } else if (args[i].equals("-embed")) {
                embed = true;
            }
        }
        initVars(homepage, javaApp, preferredSize);
        if (!embed) {
            launch(args);
        } else {
            newBrowser(homepage);
        }
    }

    static URL homepage;
    static Dimension preferredSize = new Dimension(750, 400);
    static Object javaApp;

    private final static Monitor monitorInit = new Monitor();

    /**
     * initialize default settings.
         * @param homepage
         * @param javaApp
         * @param preferredSize
         * @return 
     */
    public static Monitor initVars(URL homepage, Object javaApp, Dimension preferredSize) {
        JXAWebView.preferredSize = preferredSize;
        JXAWebView.javaApp = javaApp;
        JXAWebView.homepage = homepage;
        setLocalizedBundle(new LocalizedResources(), "back", new int[]{
            UIMessage.ABORT_TYPE,
            UIMessage.MAIL_TYPE,
            UIMessage.DOCUMENT_TYPE,
            UIMessage.DONATE_TYPE
        }, new String[]{
            "url_logout_cap",
            "url_contactus_cap",
            "url_privacy_cap",
            "url_purchase_cap"
        }, new String[]{
            "url_logout",
            "url_contactus",
            "url_privacy",
            "url_purchase"
        });
        securizedLock_locked = UIMessage._getIconResourceURL(UIMessage.LOCK_TYPE, true);
        securizedLock_open = UIMessage._getIconResourceURL(UIMessage.WEB_TYPE, true);
        return monitorInit;
    }
    static LocalizedResources bundle;

        /**
         *
         * @return
         */
        public static LocalizedResources getBundle() {
        return bundle;
    }

        /**
         *
         * @return
         */
        public static String[] getLinksCaptions_bunkey() {
        return linksCaptions_bunkey;
    }

        /**
         *
         * @return
         */
        public static URL[] getLinksImages() {
        return linksImages;
    }

        /**
         *
         * @return
         */
        public static String[] getLinksUrls_bunkey() {
        return linksUrls_bunkey;
    }

        /**
         *
         * @return
         */
        public static String getBack_bunkey() {
        return back_bunkey;
    }

    static String back_bunkey;
    static URL[] linksImages;
    static String[] linksCaptions_bunkey;
    static String[] linksUrls_bunkey;
    static URL securizedLock_open, securizedLock_locked;

    /**
         * @param bundle
         * @param back_bunkey
         * @param linksUIMessage_Type
         * @param linksCaptions_bunkey
         * @param linksUrls_bunkey
     * @see Browser#localizedBundle(net.sf.jiga.xtended.ui.LocalizedResources,
     * java.lang.String, java.net.URL[], java.lang.String[], java.lang.String[])
     */
    public static void setLocalizedBundle(LocalizedResources bundle, String back_bunkey, int[] linksUIMessage_Type, String[] linksCaptions_bunkey, String[] linksUrls_bunkey) {
        URL[] imagesFiles = new URL[linksUIMessage_Type.length];
        for (int i = 0; i < linksUIMessage_Type.length; i++) {
            imagesFiles[i] = UIMessage._getIconResourceURL(linksUIMessage_Type[i], true);
        }
        setLocalizedBundle(bundle, back_bunkey, imagesFiles, linksCaptions_bunkey, linksUrls_bunkey);
    }

    /**
         * @param bundle
         * @param linksImages
         * @param back_bunkey
         * @param linksCaptions_bunkey
         * @param linksUrls_bunkey
     * @see Browser#localizedBundle(net.sf.jiga.xtended.ui.LocalizedResources,
     * java.lang.String, java.net.URL[], java.lang.String[], java.lang.String[])
     */
    public static void setLocalizedBundle(LocalizedResources bundle, String back_bunkey, URL[] linksImages, String[] linksCaptions_bunkey, String[] linksUrls_bunkey) {
        JXAWebView.bundle = bundle;
        JXAWebView.back_bunkey = back_bunkey;
        JXAWebView.linksImages = linksImages;
        JXAWebView.linksCaptions_bunkey = linksCaptions_bunkey;
        JXAWebView.linksUrls_bunkey = linksUrls_bunkey;
    }

        /**
         *
         * @param securizedLock_locked_image
         * @param securizedLock_open_image
         */
        public static void setSecurizedLock(URL securizedLock_locked_image, URL securizedLock_open_image) {
        JXAWebView.securizedLock_locked = securizedLock_locked_image;
        JXAWebView.securizedLock_open = securizedLock_open_image;
    }

    private static boolean initialized = false;

        /**
         *
         * @param load
         * @return
         * @throws ExecutionException
         */
        public static Browser newEmbeddedBrowser(final URL load) throws ExecutionException {
        return newEmbeddedBrowser(load, null);
    }

    /**
     * 
     */
    private static WeakHashMap<URL, BufferedImage> img_cache = new WeakHashMap<URL, BufferedImage>();

    /**
     * a small lock icon for secure connections alert
         * @param uimessage_type
         * @return 
     */
    public static BufferedImage getImageSmallIcon(int uimessage_type) {
        URL url = UIMessage._getIconResourceURL(uimessage_type, true);
        BufferedImage img = img_cache.get(url);
        try {
            if (img == null) {
                img = ImageIO.read(url);
                img_cache.put(url, img);
            }
        } catch (IOException ex) {
            Logger.getLogger(JXAenvUtils.LOGGER_NAME).log(Level.WARNING, null, ex);
        } finally {
            return img;
        }
    }

        /**
         *
         * @param load
         * @param config
         * @return
         * @throws ExecutionException
         */
        public static Browser newEmbeddedBrowser(final URL load, final PopupFeatures config) throws ExecutionException {
        final JFXPanel stage = new JFXPanel();
        final FutureTask<Browser> javafx = new FutureTask<Browser>(new Callable<Browser>() {

            public Browser call() throws Exception {
                Browser browser = new Browser(stage, load, config);
                stage.setScene(browser.createScene());
                synchronized (monitorInit) {
                    monitorInit.notify();
                    return browser;
                }
            }
        });
        if (initialized) {
            throw new JXAException("Platform JavaFX was alreay initialized and cannot be reset.");
        }
        if (Platform.isFxApplicationThread()) {
            javafx.run();
        } else {
            Platform.runLater(new Runnable() {

                public void run() {
                    javafx.run();
                }
            });
        }
        try {
            synchronized (monitorInit) {
                while (!javafx.isDone()) {
                    monitorInit.wait(100);
                }
                return javafx.get();
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(JXAWebView.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

        /**
         *
         * @param browser
         * @return
         */
        public static JProgressBar newProgressBar(final Browser browser) {
        final JProgressBar progressBar = new JProgressBar();
        progressBar.setStringPainted(false);
        Runnable javafx = new Runnable() {

            public void run() {
                browser.webEngine.getLoadWorker().progressProperty().addListener(new ChangeListener<Number>() {

                    public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                        if (browser.webEngine.getLoadWorker().getProgress() != -1) {
                            progressBar.setValue((int) Math.round(browser.webEngine.getLoadWorker().getProgress() * 100.));
                        }
                    }
                });
                browser.webEngine.getLoadWorker().runningProperty().addListener(new ChangeListener<Boolean>() {

                    public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                        progressBar.setVisible(t1);
                    }
                });
            }
        };
        if (Platform.isFxApplicationThread()) {
            javafx.run();
        } else {
            Platform.runLater(javafx);
        }
        return progressBar;
    }

        /**
         *
         * @param load
         * @return
         * @throws ExecutionException
         */
        public static Browser newBrowser(URL load) throws ExecutionException {
        return newBrowser(load, null);
    }

        /**
         *
         * @param load
         * @param config
         * @return
         * @throws ExecutionException
         */
        public static Browser newBrowser(URL load, PopupFeatures config) throws ExecutionException {
        return newBrowser(load, config, null);
    }

        /**
         *
         * @param load
         * @param config
         * @param parent
         * @return
         * @throws ExecutionException
         */
        public static Browser newBrowser(URL load, PopupFeatures config, Window parent) throws ExecutionException {
        final JFrame frame = new JFrame();
        JPanel panel = new JPanel(new BorderLayout(), true);

        final Browser browser = newEmbeddedBrowser(load, config);
      
        frame.setContentPane(panel);
        if (config != null && !config.isResizable()) {
            frame.setResizable(false);
        }
        if (config != null && !config.hasStatus()) {
        } else {
            JProgressBar progressBar = newProgressBar(browser);
            panel.add(progressBar, BorderLayout.NORTH);
        }
        panel.add(browser.getJFXPanel(), BorderLayout.CENTER);
        frame.setPreferredSize(preferredSize);
        frame.setUndecorated(false);
        frame.setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
        frame.pack();
        frame.setLocationRelativeTo(parent);
        frame.setVisible(true);  
        /** browser title  */
        Runnable javafx = new Runnable() {

            public void run() {
                browser.webEngine.titleProperty().addListener(new ChangeListener<String>() {

                    public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                        if (frame != null) {
                            frame.setTitle(t1);                            
                            if (browser.webEngine.getLocation().startsWith("https")) {
                                frame.setIconImage(getImageSmallIcon(UIMessage.LOCK_TYPE));
                            } else {
                                frame.setIconImage(getImageSmallIcon(UIMessage.WEB_TYPE));
                            }
                        }
                    }
                });
            }
        };
        if (Platform.isFxApplicationThread()) {
            javafx.run();
        } else {
            Platform.runLater(javafx);
        }
        return browser;
    }

    /**
     * JavaFX WebView implementation. With a Toolbar. Configuration by
     * {@link Browser#localizedBundle(net.sf.jiga.xtended.ui.LocalizedResources, java.lang.String, java.net.URL[], java.lang.String[], java.lang.String[])}.
     */
    public static class Browser extends Region {

            /**
             *
             * @return
             */
            public WebView getView() {
            return view;
        }

            /**
             *
             * @return
             */
            public WebEngine getWebEngine() {
            return webEngine;
        }

        /**
         * @default net/sf/jiga/xtended/ui/BrowserToolbar.css
         */
        String stylesheet = "net/sf/jiga/xtended/ui/BrowserToolbar.css";

        Color sceneColor = Color.web("#666970");

            /**
             *
             * @param sceneColor
             */
            public void setSceneColor(Color sceneColor) {
            this.sceneColor = sceneColor;
        }

            /**
             *
             * @param resourcepath
             */
            public void setStylesheet(String resourcepath) {
            stylesheet = resourcepath;
        }

        private Scene createScene() {
            /* create scene */
            Scene scene = new Scene(this, preferredSize.width, preferredSize.height, sceneColor);
            /* apply CSS style */
            scene.getStylesheets().add(stylesheet);

            webEngine.getLoadWorker().stateProperty().addListener(new javafx.beans.value.ChangeListener<Worker.State>() {
                @Override
                public void changed(ObservableValue<? extends Worker.State> ov,
                        Worker.State oldState, Worker.State newState) {
                    if (newState == Worker.State.SUCCEEDED) {

                        /**
                         * lock pic
                         */
                        Document d = webEngine.getDocument();
                        if (d != null) {
                            String u = d.getDocumentURI();
                            toggleSecurizedLock(u.startsWith("https"));
                        }

                        /**
                         * Java/JS bridge
                         */
                        if (javaApp != null) {
                            JSObject win = (JSObject) webEngine.executeScript("window");
                            win.setMember("sf3jxakn_app", javaApp);
                        }
                    }
                }
            });
            webEngine.locationProperty().addListener(new ChangeListener<String>() {

                public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                    /**
                     * log URLs
                     */
                    Logger.getLogger(JXAenvUtils.LOGGER_NAME).log(Level.INFO, "WebView browsing to " + t1);
                }
            });
            return scene;
        }

        private HBox toolBar;

            /**
             *
             * @param parent
             * @param load
             * @param config
             */
            public Browser(final Object parent, URL load, final PopupFeatures config) {
            super();
            if (parent instanceof Application) {
                this.application = (Application) parent;
            }
            if (parent instanceof JFXPanel) {
                this.jfxPanel = (JFXPanel) parent;
            }
            localizedBundle(JXAWebView.bundle, JXAWebView.back_bunkey, JXAWebView.linksImages, JXAWebView.linksCaptions_bunkey, JXAWebView.linksUrls_bunkey);
            /*apply the styles*/
            getStyleClass().add("browser");

            /*handle popup windows*/
            webEngine.setCreatePopupHandler(
                    new Callback<PopupFeatures, WebEngine>() {
                        @Override
                        public WebEngine call(PopupFeatures config) {
                            try {
                                if (parent instanceof Window) {
                                    return newBrowser(homepage, config, (Window) parent).webEngine;
                                } else {
                                    return newBrowser(homepage, config).webEngine;
                                }
                            } catch (Exception ex) {
                                Logger.getLogger(JXAWebView.class.getName()).log(Level.SEVERE, null, ex);
                                return null;
                            }
                        }
                    }
            );

            /**
             * javascript alert()
             */
            webEngine.setOnAlert(new EventHandler<WebEvent<String>>() {

                public void handle(WebEvent<String> t) {
                    UIMessage.showLightPopupMessage(new JLabel(t.getData()), null, jfxPanel, UIMessage.UI_CENTER);
                }
            });
            /**
             * error handle
             */
            webEngine.setOnError(new EventHandler<WebErrorEvent>() {

                public void handle(final WebErrorEvent t) {
                    UIMessage.showPopupMessage(new JLabel("<html><u>Error :</u> " + t.getMessage() + "</html>"), new AbstractAction("More...") {

                        public void actionPerformed(java.awt.event.ActionEvent e) {
                            JXAenvUtils._popExceptionToUser(false, Thread.currentThread(), t.getException());
                        }
                    }, jfxPanel, UIMessage.UI_CENTER);
                }
            });

            /*process history*/
            final WebHistory history = webEngine.getHistory();
            history.getEntries().addListener(new ListChangeListener<WebHistory.Entry>() {
                @Override
                public void onChanged(Change<? extends WebHistory.Entry> c) {
                    c.next();
                    for (Entry e : c.getRemoved()) {
                        comboBox.getItems().remove(e.getUrl());
                    }
                    for (Entry e : c.getAddedSubList()) {
                        comboBox.getItems().add(e.getUrl());
                    }
                }
            });

            comboBox.setPrefWidth(150);
            /*set the behavior for the history combobox               */
            comboBox.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent ev) {
                    int offset
                            = comboBox.getSelectionModel().getSelectedIndex()
                            - history.getCurrentIndex();
                    history.go(offset);
                }
            });

            /* process page loading*/
            webEngine.getLoadWorker().stateProperty().addListener(new ChangeListener<State>() {
                @Override
                public void changed(ObservableValue<? extends State> ov,
                        State oldState, State newState) {
                    toolBar.getChildren().remove(showPrevDoc);
                    if (newState == State.SUCCEEDED) {
                        JSObject win = (JSObject) webEngine.executeScript("window");
                        win.setMember("sf3jxakn_app", javaApp);
                        toolBar.getChildren().add(showPrevDoc);
                    }
                }
            }
            );

            /* load the home page        */
            webEngine.load(load.toExternalForm());

            /*add components*/
            if (config != null && !config.hasToolbar()) {
            } else {
                getChildren().add(toolBar);
            }
            getChildren().add(view);
        }

        /**
         * fill toolbar with links from LocalizedResources: 3 parameters must be
         * same length, one entry for each link in toolbar
         *
             * @param bundle
             * @param back_bunkey
             * @param linksImages
             * @param linksCaptions_bunkey
             * @param linksUrls_bunkey
         * @see UIMessage
         */
        public final void localizedBundle(LocalizedResources bundle, String back_bunkey, URL[] linksImages, String[] linksCaptions_bunkey, String[] linksUrls_bunkey) {
            text = bundle;
            if (linksUrls_bunkey.length != linksCaptions_bunkey.length) {
                throw new JXAException("invalid parameters length");
            }
            makeToolbar(back_bunkey, linksImages, linksCaptions_bunkey, linksUrls_bunkey);
        }

        private void makeToolbar(String back_bunkey, URL[] linksImages, String[] linksCaptions_bunkey, String[] linksUrls_bunkey) {
            hpls = new Hyperlink[linksCaptions_bunkey.length];
            images = new Image[linksImages.length];
            for (int i = 0; i < linksCaptions_bunkey.length; i++) {
                /* create hyperlinks*/
                Hyperlink hpl = hpls[i] = new Hyperlink(text.bundle.getString(linksCaptions_bunkey[i]));
                try {
                    Image image = images[i] = new Image(linksImages[i].openStream());
                    hpl.setGraphic(new ImageView(image));
                } catch (IOException ex) {
                    Logger.getLogger(JXAenvUtils.LOGGER_NAME).log(Level.SEVERE, null, ex);
                } finally {
                    final String url = text.bundle.getString(linksUrls_bunkey[i]);

                    /* process event */
                    hpl.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent e) {
                            /**
                             * STOP action
                             */
                            if (url.equals("jxa://stop")) {
                                webEngine.getLoadWorker().cancel();
                            } else {
                                webEngine.load(url);
                            }
                        }
                    });
                }
            }
            /*create the toolbar*/
            if (toolBar != null) {
                toolBar.getChildren().removeAll(toolBar.getChildren());
            } else {
                toolBar = new HBox();
                toolBar.getStyleClass().add("browser-toolbar");
            }
            toolBar.setAlignment(Pos.CENTER);
            /**
             * toggle secure lock
             */
            securizedLock = new Button();
            toggleSecurizedLock(false);
            toolBar.getChildren().add(securizedLock);

            toolBar.getChildren().add(createSpacer());
            toolBar.getChildren().add(comboBox);
            toolBar.getChildren().addAll(hpls);
            toolBar.getChildren().add(createSpacer());
            /*set action for the button*/
            showPrevDoc = new Button(text.bundle.getString(back_bunkey));
            showPrevDoc.setOnAction(new EventHandler() {
                @Override
                public void handle(Event t) {
                    if (webEngine.getHistory().getCurrentIndex() > 0) {
                        webEngine.getHistory().go(-1);
                    }
                }
            });

        }

            /**
             *
             * @param locked
             */
            public void toggleSecurizedLock(boolean locked) {
            try {
                if (!locked) {
                    securizedLock.setGraphic(new ImageView(new Image(securizedLock_open.openStream())));
                } else {
                    securizedLock.setGraphic(new ImageView(new Image(securizedLock_locked.openStream())));
                }
            } catch (IOException ex) {
                Logger.getLogger(JXAenvUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        final ImageView selectedImage = new ImageView();
        Hyperlink[] hpls;
        Image[] images;
        final WebView view = new WebView();
        final WebEngine webEngine = view.getEngine();
        Button showPrevDoc, securizedLock;
        final WebView smallView = new WebView();
        final ComboBox comboBox = new ComboBox();

            /**
             *
             * @return
             */
            public ComboBox getComboBox() {
            return comboBox;
        }

        Application application = null;
        LocalizedResources text;

            /**
             *
             * @return
             */
            public Application getApplication() {
            return application;
        }

        private Node createSpacer() {
            Region spacer = new Region();
            HBox.setHgrow(spacer, Priority.ALWAYS);
            return spacer;
        }

        @Override
        protected void layoutChildren() {
            double w = getWidth();
            double h = getHeight();
            double tbHeight = toolBar.prefHeight(w);
            layoutInArea(view, 0, 0, w, h - tbHeight, 0, HPos.CENTER, VPos.CENTER);
            layoutInArea(toolBar, 0, h - tbHeight, w, tbHeight, 0, HPos.CENTER, VPos.CENTER);
        }

        @Override
        protected double computePrefWidth(double width) {
            return JXAWebView.preferredSize.getWidth();
        }

        @Override
        protected double computePrefHeight(double height) {
            return JXAWebView.preferredSize.getHeight();
        }
        private JFXPanel jfxPanel = null;

        private JFXPanel getJFXPanel() {
            return jfxPanel;
        }
    }
}
