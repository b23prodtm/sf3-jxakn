package net.sf.jiga.xtended.kernel;

import java.util.Stack;

/**
 * Monitors <br>Code HINT : it's always worth to NOTIFY() before leaving any
 * synchronized block, it avoids unnotified WAIT()'s getting the system hung
 * out.
 *
 * @author www.b23prodtm.info *
 */
public class Monitor {

        /**
         * the hash code to identify this Object
         */
        public long _hash = System.nanoTime();
        private Stack<Long> queue = new Stack<Long>();

        private void queue() {
                if (!queue.contains(Thread.currentThread().getId())) {
                        queue.add(0, Thread.currentThread().getId());
                }
        }

        private boolean wakeIfFirst() {
                long tId = queue.peek();
                if (Thread.currentThread().getId() == tId) {
                        queue.pop();
                        return true;
                }
                return false;
        }

        /**
         * a FIFO monitored queue , that is if multiple threads wait
         * simultanously on this monitor, the next {
         * @link #notifyAll()} will wake on threads as their queue list order,
         * i.e. as in the order they acquired the lock on this Monitor. notice :
         * wait(0) is run first.
         */
        /**
         * a FIFO monitored queue , that is if multiple threads wait
         * simultanously on this monitor, the next {@link #notifyAll()} will
         * wake on threads as their queue list order, i.e. as in the order they
         * acquired the lock on this Monitor.notice : wait(0) is run first.
         *
         * @param millisToWaitOnWake
         * @throws java.lang.InterruptedException
         */
        public synchronized void waitOnQueue(long millisToWaitOnWake) throws InterruptedException {
                waitOnQueue(0, millisToWaitOnWake);
        }

        /**
         * a FIFO monitored queue, that is if multiple thread wait simultanously
         * on this monitor, the next {@link #notifyAll()} will wake on threads
         * as their queue list order notice : wait(miilisToWait)
         *
         * @param millisToWait
         * @param millisToWaitOnWake
         * @throws java.lang.InterruptedException
         */
        protected synchronized void waitOnQueue(long millisToWait, long millisToWaitOnWake) throws InterruptedException {
                queue();
                /**
                 * wait as always
                 */
                wait(millisToWait);
                /**
                 * stay on queue, as long as there are other Threads left on top
                 * of the queue
                 */
                while (!wakeIfFirst()) {
                        notify();
                        wait(millisToWaitOnWake);
                }
        }

        /**
         *
         * @param obj
         * @return
         */
        @Override
        public boolean equals(Object obj) {
                return obj == null ? false : obj.hashCode() == hashCode();
        }

        /**
         *
         * @return
         */
        @Override
        public int hashCode() {
                return (int) _hash;
        }
}
