/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sf.jiga.xtended.ui;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * @author www.b23prodtm.info
 */
public class LocalizedResources {

        /**
         * current displayed language
         */
        public Locale language = Locale.getDefault();
        /**
         * various available languages for the ResourceBundle
         */
        public Locale[] languages = new Locale[]{new Locale("en"), new Locale("fr"), new Locale("de")};

        /**
         *
         */
        public ResourceBundle bundle;

        /**
         * constructs an array of the specified key in {@link #bundle localized}
         * ResourceBundle
         *
         * @param key ResourceBundle key
         * @param n number of elements for the key-n (e.g. mylabel-0, mylabel-1
         * are 2 elements for key mylabel).
         * @return
         */
        public String[] localizedGetStringArray(String key, int n) {
                ArrayList<String> v = new ArrayList<String>();
                for (Enumeration<String> keys = bundle.getKeys(); keys.hasMoreElements();) {
                        String k = keys.nextElement();
                        for (int i = 0; i < n; i++) {
                                if (k.equals(key + "-" + i)) {
                                        if (i >= v.size()) {
                                                v.add(bundle.getString(k));
                                        } else {
                                                v.add(i, bundle.getString(k));
                                        }
                                }
                        }
                }
                return v.toArray(new String[]{});
        }

        /**
         *
         */
        public String bundleFile;

        /**
         *
         *
         * @param locale
         * @return the localized ResourceBundle with the correct available
         * locale
         */
        public final ResourceBundle getLocalizedRB(Locale locale) {
                return ResourceBundle.getBundle(bundleFile, locale);
        }

        /**
         * locale resource file (net/sf/jiga/xtended/ui/loc.properties)
         */
        public LocalizedResources() {
                this("net.sf.jiga.xtended.ui.loc");
        }

        /**
         *
         * @param bundleFile
         */
        public LocalizedResources(String bundleFile) {
                this.bundleFile = bundleFile;
                this.bundle = getLocalizedRB(language);
        }

}
