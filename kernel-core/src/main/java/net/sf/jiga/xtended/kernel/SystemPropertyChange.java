/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sf.jiga.xtended.kernel;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Change a (system) property and dispatch event to the list of registered listeners. These PropertyChangeListener should update 
 * corresponding API variable or do some defined tasks.
 * Singleton class. Access members through {@link #INSTANCE}.
 * @author wwwb23prodtminfo <b23prodtm at sourceforge.net>
 */
public class SystemPropertyChange {

        /**
         *
         */
        public List<PropertyChangeListener> listeners = Collections.synchronizedList(new ArrayList<PropertyChangeListener>());

        private SystemPropertyChange() {
        }

        /**
         *
         */
        public final static SystemPropertyChange INSTANCE = new SystemPropertyChange();

        /**
         *
         * @param l
         */
        public void add(PropertyChangeListener l) {
                listeners.add(l);
        }

        /**
         *
         * @param ev
         */
        public void dispatchEvent(PropertyChangeEvent ev) {
                synchronized (listeners) {
                        for (PropertyChangeListener l : listeners) {
                                l.propertyChange(ev);
                        }
                }
        }
}
