/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sf.jiga.xtended.kernel;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilePermission;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.FileChannel;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import javax.imageio.ImageIO;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author tiana
 */
public class FileHelper {

    /**
     * writeable java.io.tmpdir or "." directory
     */
    public static File _USERHOMEDIRECTORY = _findHomeDirectory();
    /*
     * private final static Set<SoftReference<File>> _tmpFiles =
     * Collections.synchronizedSet(new HashSet<SoftReference<File>>());
     */
    /**
     *
     */
    public static File _USERHOMESTOREDIRECTORY = _makeStoreDirectory(_USERHOMEDIRECTORY);
    /**
     *
     */
    public static final int FILE_READ = 1;
    /**
     *
     */
    public static Monitor fileThreads = new Monitor();
    /**
     * all temporary files created from JXA should end with this suffix
     *
     * @default .jxa
     */
    public static final String _tmpFilesSuffix = ".jxa.tmp";
    /**
     *
     */
    private static boolean _ImageIOUseCache = false;
    /**
     *
     */
    public static final int FILE_EXECUTE = 8;
    /**
     *
     */
    public static final int FILE_DELETE = 4;
    /**
     * for file i/o operations
     *
     * @default 65536 (64k)
     *
     * @see RandomAccessFile
     */
    public static int _SMALLBUFFFER_SIZE = 65536;
    /**
     * writeable user.home or "." directory
     */
    public static File _TMPDIRECTORY = _findTempDirectory();
    /**
     * writeable directory for native libraries
     */
    public static File _LIBDIRECTORY = new File(ExtensionsInstaller._findExtPath(true));
    /**
     *
     */
    public static List<Long> abortedThreads = new ArrayList<Long>();
    /**
     * timeout delay for streams connections in ms
     *
     * @default 5'000 ms
     */
    public static long _streamsTimeout = 5000;
    /**
     *
     */
    public static File _ImageIOCache = new File(_TMPDIRECTORY, "JavaImageIO");
    /**
     * the tmp directories set
     */
    protected static final Set<File> _tmpDir = Collections.synchronizedSet(new HashSet<File>(Collections.singleton(_TMPDIRECTORY)));
    /**
     * for read/write from url downloads
     *
     * @default 65536 * 16 = 1MB
     */
    public static int _BIGBUFFER_SIZE = 65536 * 16;
    /**
     *
     */
    public static final int FILE_WRITE = 2;
    /**
     * writeable java.ext.dirs directory
     */
    public static File _EXTDIRECTORY = new File(ExtensionsInstaller._findExtPath(false));

    /**
     * CAUTION : this function will attempt to delete all files in the specified
     * directory (but not the sub-dirs)
     *
     * @param suffix the suffix for filenames (".[a-z|0-9]*")
     * @param dir
     * @throws IllegalArgumentException
     */
    public static void _eraseTmpFiles(final String suffix, File dir) throws IllegalArgumentException {
        if (!suffix.matches("\\..*")) {
            throw new IllegalArgumentException("suffix must be \".someextension\"");
        }
        if (dir.isDirectory()) {
            for (File f : dir.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.endsWith(suffix);
                }
            })) {
                _eraseTmpFiles(suffix, f);
            }
        } else if (dir.isFile() && _accessFilePermitted(dir, FILE_DELETE) && dir.getName().endsWith(suffix)) {
            dir.delete();
        }
    }

    /**
     * {@linkplain String#split(String)} won't fail on Windows with the
     * File.separator as splitter ex. The method computes as follows :
     * <pre>String splitter = File.separator;
     * if(splitter.equals("\\")) {
     * splitter = "\\\\";
     * }
     * return splitter;</pre>
     *
     * @return
     */
    public static String _quotedFileSeparator() {
        String splitter = File.separator;
        if (splitter.equals("\\")) {
            splitter = "\\\\";
        }
        return splitter;
    }

    /**
     * secure copy of src file to dst file (first a temp. file is created and
     * filled with the data)
     *
     * @param src
     * @param bigBuffer
     * @param progress
     * @param dst
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void _fileCopy(File src, File dst, boolean progress, boolean bigBuffer) throws FileNotFoundException, IOException {
        long id = 0;
        _fileCopy(src, dst, progress, id, bigBuffer);
    }

    /**
     * secure copy of src file to dst file (first a temp. file is created and
     * filled with the data)
     *
     * @param src
     * @param dst
     * @param progress
     * @param bigBuffer
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void _fileCopy(InputStream src, File dst, boolean progress, boolean bigBuffer) throws FileNotFoundException, IOException {
        long id = 0;
        _fileCopy(src, dst, progress, id, bigBuffer);
    }

    /**
     * secure copy of src file to dst file (first a temp. file is created and
     * filled with the data)
     *
     * @param src
     * @param dst
     * @param bigBuffer
     * @param progress
     * @throws FileNotFoundException
     * @throws IOException
     * @throws HttpException
     */
    public static void _fileCopy(URL src, File dst, boolean progress, boolean bigBuffer) throws FileNotFoundException, IOException, HttpException {
        long id = 0;
        _fileCopy(src, dst, progress, id, bigBuffer);
    }

    /**
     * secure copy of src file to dst file (first a temp. file is created and
     * filled with the data)
     *
     * @param src
     * @param dst
     * @param progress
     * @param progressID
     * @param bigBuffer
     * @throws IOException
     */
    public static void _fileCopy(final File src, final File dst, final boolean progress, final long progressID, final boolean bigBuffer) throws IOException {
        Copy.__fileCopy(src, dst, progress, progressID, bigBuffer);
    }

    /**
     * secure copy of src file to dst file (first a temp. file is created and
     * filled with the data)
     *
     * @param src an InputStream, after the copy, the inputstream is left at the
     * last read postion and not closed UNLESS it is a BufferedInputStream (so
     * don't specify any such stream if you don't want it to be closed !)
     * @param dst
     * @param progress
     * @param progressID
     * @param bigBuffer
     * @throws IOException
     */
    public static void _fileCopy(final InputStream src, final File dst, final boolean progress, final long progressID, final boolean bigBuffer) throws IOException {
        Copy.__fileCopy(src, dst, progress, progressID, bigBuffer);
    }

    /**
     * secure copy of src file to dst file (first a temp. file is created and
     * filled with the data)
     *
     * @param src
     * @param dst
     * @param progressID
     * @param progress
     * @param bigBuffer
     * @throws IOException
     */
    public static void _fileCopy(final URL src, final File dst, final boolean progress, final long progressID, final boolean bigBuffer) throws IOException {
        Copy.__fileCopy(src, dst, progress, progressID, bigBuffer);
    }

    /**
     *
     *
     * @param fd
     */
    public static void _makeReadable(File fd) {
        __makeReadable(fd, false);
    }

    /**
     *
     * @param fd the value of fd
     * @param ownerOnly the value of ownerOnly
     */
    private static boolean __makeWritable(File fd, boolean ownerOnly) {
        // write access was granted, so it may be set writeable for the current user
        try {
            if (!_accessFilePermitted(fd, FILE_WRITE) && !env.JAVA_VERSION_5.isEnv()) {

                return JXAenvUtils._callback("setWritable", fd, new Object[]{true, ownerOnly}, new Class[]{boolean.class, boolean.class});
            } else {
                if (fd.getAbsolutePath().endsWith(File.separator)) {
                    fd.mkdirs();
                }
                if (fd.isDirectory()) {
                    /**
                     * may not be modified
                     */
                } else if (!fd.exists()) {
                    /* it s a new file */
                    fd.getParentFile().mkdirs();
                    fd.createNewFile();
                    return true;
                }
                if (DebugMap._getInstance().isDebugLevelEnabled(DebugMap._getInstance()._VOID)) {
                    System.err.println("File " + fd + " is not writeable.");
                }
            }
        } catch (NoSuchMethodException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (InvocationTargetException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            return false;
        }
    }

    /**
     * finds a directory that is allowed to read, write and execute.
     *
     * @param freeList
     * @return
     */
    public static File _findFreeDirectory(List<File> freeList) {
        File freeDir = null;
        for (File f : freeList) {
            if (f.isDirectory()) {
                File access = f;
                if (_accessFilePermitted(access, FILE_READ | FILE_WRITE)) {
                    freeDir = f;
                    break;
                }
            }
        }
        if (freeDir == null) {
            if (JXAenvUtils._debugSys) {
                System.err.println(JXAenvUtils.log("Sorry ! It's unable to find a writeable directory. Leaving now.", JXAenvUtils.LVL.SYS_ERR, null));
            }
            System.exit(1);
        }
        return freeDir;

    }

    /**
     *
     * @param fd the value of fd
     * @param ownerOnly the value of ownerOnly
     */
    private static boolean __makeReadable(File fd, boolean ownerOnly) {
        if (!env.JAVA_VERSION_5.isEnv()) {
            try {
                return JXAenvUtils._callback("setReadable", fd, new Object[]{true, ownerOnly}, new Class[]{boolean.class, boolean.class
                }
                );
            } catch (NoSuchMethodException ex) {
                ex.printStackTrace();
            } catch (IllegalAccessException ex) {
                ex.printStackTrace();
            } catch (InvocationTargetException ex) {
                ex.printStackTrace();
            }
        } else if (DebugMap._getInstance().isDebugLevelEnabled(DebugMap._getInstance()._VOID)) {
            System.out.println("setReadable not available from this version of Java. " + fd);
        }
        return false;
    }

    /**
     *
     *
     * @param fd
     */
    public static void _makeWritable(File fd) {
        __makeWritable(fd, false);
    }

    /**
     *
     *
     * @param fd
     */
    public static void _makeReadableOwnerOnly(File fd) {
        __makeReadable(fd, true);
    }

    /**
     *
     *
     * @param fd
     */
    public static void _makeWritableOwnerOnly(File fd) {
        __makeWritable(fd, true);
    }

    /**
     *
     *
     * @param s
     * @param d
         * @param syncFlush
     * @throws IOException
     */
    public static void ___rawfileCopy(File s, File d, boolean syncFlush) throws IOException {
        FileChannel source = null;
        FileChannel destination = null;
        if (!d.exists()) {
            d.getParentFile().mkdirs();
            d.createNewFile();
        }
        RandomAccessFile raf = new RandomAccessFile(d, "rw");
        source = new FileInputStream(s).getChannel();
        destination = new FileOutputStream(raf.getFD()).getChannel();
        destination.transferFrom(source, 0, source.size());
        source.close();
        destination.force(syncFlush);
        destination.close();
        raf.close();
    }

    /**
     * This method uses the Java Security Framework to check the FilePermission
     * for some file.
     *
     * @param file the file path you want to check security
     * @param fileMode the access-mode : a bitwise-OR combination of
     * {@linkplain #FILE_READ}, {@linkplain #FILE_WRITE}, {@linkplain #FILE_EXECUTE}
     * or {@linkplain #FILE_DELETE}
     * @return true or false, whether the FilePermission is returning ALL OK or
     * not for the specified fileMode, resp.
     * @throws SecurityException if the file cannot be accessed using one, more
     * or all the specified access modes
     * @see FilePermission
     */
    public static boolean _accessFilePermitted(final File file, final int fileMode) {
        return AccessController.doPrivileged(new PrivilegedAction<Boolean>() {
            public Boolean run() {
                return __accessFilePermitted(file, fileMode);
            }
        }, JXAenvUtils.acc);
    }

    /**
     *
     * @param url the value of url
         * @return 
     */
    public static String _getURLFilename(URL url) {
        return url.getFile().substring(url.getFile().lastIndexOf("/") + 1);
    }

    /**
     *
     * @param base the value of base
     */
    private static File _makeStoreDirectory(final File base) {
        try {
            return AccessController.doPrivileged(new PrivilegedExceptionAction<File>() {
                public File run() throws IOException {
                    String basePath = base.getPath();
                    if (!base.isDirectory()) {
                        basePath = base.getParent();
                    }
                    if (env.OS_WINDOWS.isEnv()) {
                        try {
                            File userAppData = new File(System.getenv("APPDATA"), "Java");
                            if (userAppData.canWrite()) {
                                basePath = userAppData.getPath();
                            }
                        } catch (SecurityException e) {
                            if (JXAenvUtils._debugSys) {
                                e.printStackTrace();
                            }
                        }
                    }
                    File store = new File(basePath, ".JXAStore");
                    store.mkdirs();
                    _makeWritable(new File(basePath));
                    return store;
                }
            }, JXAenvUtils.acc);
        } catch (PrivilegedActionException ex) {
            if (JXAenvUtils._debugSys) {
                ex.getException().printStackTrace();
            }
            return null;
        }
    }

    /**
     * This method uses the Java Security Framework to check the FilePermission
     * for some file.
     *
     * @param file the file path you want to check security
     * @param fileMode the access-mode : a bitwise-OR combination of
     * {@linkplain #FILE_READ}, {@linkplain #FILE_WRITE}, {@linkplain #FILE_EXECUTE}
     * or {@linkplain #FILE_DELETE}
     * @return true or false, whether the FilePermission is returning ALL OK or
     * not for the specified fileMode, resp.
     * @throws SecurityException if the file cannot be accessed using one, more
     * or all the specified access modes
     * @see FilePermission
     */
    static boolean __accessFilePermitted(File file, int fileMode) {
        if (file == null || fileMode == 0) {
            return false;
        }
        /**
         * see up parent folder
         */
        if (!file.exists()) {
            return __accessFilePermitted(file.getParentFile(), fileMode);
        }
        ArrayList<String> actions = new ArrayList<String>();
        if ((fileMode & FILE_READ) != 0) {
            actions.add("read");
        }
        if ((fileMode & FILE_WRITE) != 0) {
            actions.add("write");
        }
        if ((fileMode & FILE_DELETE) != 0) {
            actions.add("delete");
        }
        if ((fileMode & FILE_EXECUTE) != 0) {
            actions.add("execute");
        }
        String mode = "";
        String sep = "";
        for (String action : actions) {
            mode += sep + action;
            sep = ",";
        }
        if (file.isDirectory()) {
            file = new File(file.getAbsolutePath() + File.separator + "*");
        }
        FilePermission fp = new FilePermission(file.getAbsolutePath(), mode);
        try {
            fp.checkGuard(null);
            /**
             * additional check * if ((fileMode & FILE_READ) != 0) {
             * file.length(); } if ((fileMode & FILE_WRITE) != 0) {
             * File.createTempFile("jxaEnv_", "_createWrite", file.isDirectory()
             * ? file : file.getParentFile()).delete(); }
             */
            return true;
        } catch (Exception e) {
            if (JXAenvUtils._debugSys) {
                System.err.println(JXAenvUtils.log((file.isDirectory() ? "Folder " : "File ") + fp.getName() + " is not allowed for accessing one or more actions : " + fp.getActions() + " " + e.getMessage(), JXAenvUtils.LVL.SYS_ERR, e));
            }
            return false;
        }
    }

    /**
     * completely erase a path (directory or file)
     *
     * @param path
     */
    public static void _erase(File path) {
        try {
            _erase(path, false);
        } catch (IOException ex) {
            if (JXAenvUtils._debugSys) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * completely erase a path (directory or file)
     *
     * @param path
     * @param reportException
     * @throws IOException
     */
    public static void _erase(File path, boolean reportException) throws IOException {
        if (_accessFilePermitted(path, FILE_DELETE)) {
            if (path.isDirectory()) {
                for (File f : path.listFiles()) {
                    _erase(f, reportException);
                }
            }
            path.delete();
            if (JXAenvUtils._debugSys) {
                System.out.println("erased file path " + path);
            }
        } else {
            if (path.exists()) {
                System.err.println("unable to erase " + path);
                if (reportException) {
                    throw new IOException("File " + path + " cannot be deleted");
                }
            } else {
                if (JXAenvUtils._debugSys) {
                    System.err.println("unable to delete a file that does not exist " + path);
                }
            }
        }
    }

    /**
     * dis/enables ImageUIO caching
     *
     * @param b true or false, to enable/disable, resp.
     * @param cacheDirectory the folder where ImageIO will put the cache files
     * @default enabled, if disabled, unexpected behaviours may occur with image
     * loading.
     */
    public static void _setImageIOCacheEnabled(final boolean b, final File cacheDirectory) {
        try {
            ImageIO.setUseCache(_ImageIOUseCache = b);
            if (b) {
                cacheDirectory.mkdirs();
                _makeWritable(cacheDirectory);
                ImageIO.setCacheDirectory(cacheDirectory);
                _ImageIOCache = cacheDirectory;
            }
        } catch (Exception e) {
            if (JXAenvUtils._debugSys) {
                e.printStackTrace();
            }
            ImageIO.setUseCache(_ImageIOUseCache = false);
        }
    }

    /**
     *
     */
    static File _findTempDirectory() {
        File f = AccessController.doPrivileged(new PrivilegedAction<File>() {
            public File run() {
                ArrayList<File> tempPath = new ArrayList<File>();
                if (JXAenvUtils.rb.getString("tempPathEnabled").equalsIgnoreCase("true")) {
                    tempPath.add(new File(JXAenvUtils._getSysValue("java.io.tmpdir")));
                }
                tempPath.add(new File("."));
                return _findFreeDirectory(tempPath);
            }
        }, JXAenvUtils.acc);
        if (f == null) {
            f = new File(".");
        }
        return f;
    }

    /**
     *
     */
    static File _findHomeDirectory() {
        File f = AccessController.doPrivileged(new PrivilegedAction<File>() {
            public File run() {
                ArrayList<File> tempPath = new ArrayList<File>();
                if (JXAenvUtils.rb.getString("homePathEnabled").equalsIgnoreCase("true")) {
                    tempPath.add(new File(JXAenvUtils._getSysValue("user.home")));
                }
                tempPath.add(new File("."));
                return _findFreeDirectory(tempPath);
            }
        }, JXAenvUtils.acc);
        if (f == null) {
            f = new File(".");
        }
        return f;
    }

    /**
     * Usually, new File("/filename") returns "\filename" as a path on Windows
     * and "//filename" on Unix, this method converts the abstract path name
     * from the specified File to a resource String (i.e.
     * {@linkplain Class#getResourceAsStream(String)} can handle it).
     *
     * @param filePath
     * @return
     */
    public static String _convertToResourceString(File filePath) {
        String resource = "";
        String sep = "";
        String splitter = _quotedFileSeparator();
        if (filePath.getPath().startsWith(splitter)) {
            sep = "/";
        }
        for (String s : filePath.getPath().split(splitter)) {
            resource += sep + s;
            sep = "/";
        }
        return resource.replaceAll(splitter + splitter, splitter);
    }

    /**
     *
     *
     * @return
     */
    public static boolean _isImageIOCacheEnabled() {
        return _ImageIOUseCache;
    }

    /**
     * will create a jxa temp file, that is monitored by the env (delete on
     * exit, file suffix, etc.)
     *
     * @param prefix
     * @param dir
         * @param deleteOnExit
     * @return
     * @throws IOException
     */
    public static File _createTempFile(String prefix, File dir, boolean deleteOnExit) throws IOException {
        File f = File.createTempFile(prefix, _tmpFilesSuffix, dir);
        _tmpDir.add(dir);
        _makeReadable(f);
        _makeWritable(f);
        if (deleteOnExit) {
            f.deleteOnExit();
        }
        SpritesCacheManager._cleanup();
        return f;
    }

        /**
         * Class that encapsulates all file copy actions.
         */
        public static class Copy {

        /**
         * start recursed __fileCopy
         */
        public static final String _key_reading = "reading";

            /**
             *
             */
            public static final String _key_read_string = "string";

            /**
             *
             */
            public static final String _key_read_pgs_val = "value";

            /**
             *
             */
            public static final String _key_read_pgs_max = "max";

        private static void __fileCopy_r(RandomAccessFile src, int r, byte[] b, int rBytes, RandomAccessFile JXAtmpPath, boolean progress) throws IOException {
            r = src.read(b);
            if (r == -1) {
                return;
            }
            __fileCopy_write(r, b, rBytes, JXAtmpPath);
            if (progress) {
                readProgress.get(Thread.currentThread().getId()).put(_key_read_pgs_val, String.valueOf(rBytes));
                readProgress.get(Thread.currentThread().getId()).put(_key_read_pgs_max, String.valueOf((int) src.length()));
            }
            __fileCopy_r(src, r, b, rBytes, JXAtmpPath, progress);
        }

        private static void __fileCopy_r(InputStream src, HttpClient HTTPConnection, URLConnection connection, int r, byte[] b, int rBytes, RandomAccessFile JXAtmpPath, boolean progress) throws IOException {
            r = src.read(b);
            if (r == -1) {
                return;
            }
            __fileCopy_write(r, b, rBytes, JXAtmpPath);
            if (progress) {
                readProgress.get(Thread.currentThread().getId()).put(_key_read_string, Matcher.quoteReplacement((HTTPConnection instanceof HttpClient ? "downloaded " : "copied ") + FileUtils.byteCountToDisplaySize((long) rBytes)) + " (%1$s remaining)");
                readProgress.get(Thread.currentThread().getId()).put(_key_read_pgs_val, String.valueOf(rBytes));
                readProgress.get(Thread.currentThread().getId()).put(_key_read_pgs_max, String.valueOf((int) (connection instanceof URLConnection ? connection.getContentLength() : src.available())));
            }
            __fileCopy_r(src, HTTPConnection, connection, r, b, rBytes, JXAtmpPath, progress);
        }

        private static void __fileCopy_write(int r, byte[] b, int rBytes, RandomAccessFile JXAtmpPath) throws IOException {
            if (abortedThreads.contains(Thread.currentThread().getId())) {
                abortedThreads.remove(Thread.currentThread().getId());
                throw new IOException("File Copy was aborted by another Thread");
            }
            synchronized (fileThreads) {
                fileThreads.notify();
            }
            JXAtmpPath.write(b, 0, r);
            rBytes += r;
        }
        /**
         * end recursed
         */
        /**
         * Thread handler
         */
        public final static Map<Long, Map<String, Boolean>> readSync = new HashMap<Long, Map<String, Boolean>>();

            /**
             *
             */
            public final static Map<Long, Map<String, String>> readProgress = new HashMap<Long, Map<String, String>>();

        /**
         * start file copy on a separate thread to allow inputstream handle and
         * progress bars display
         */
        private static void __fileCopy_tStart(Runnable copyRunnable, Object src, File dst, boolean progress, long id, boolean bigBuffer) {
            try {
                Thread tCopy = new Thread(copyRunnable, "file copy " + src + " -> " + dst);
                tCopy.setPriority(tCopy.MAX_PRIORITY);
                boolean retryRequested = false;
                synchronized (fileThreads) {
                    readSync.put(tCopy.getId(), Collections.synchronizedMap(new HashMap<String, Boolean>(Collections.singletonMap(_key_reading, false))));
                    readProgress.put(tCopy.getId(), Collections.synchronizedMap(new HashMap<String, String>()));
                    readProgress.get(tCopy.getId()).put(_key_read_string, "");
                    readProgress.get(tCopy.getId()).put(_key_read_pgs_val, "0");
                    readProgress.get(tCopy.getId()).put(_key_read_pgs_max, "100");
                    if (progress) {
                        readProgress.get(tCopy.getId()).put(_key_read_string, "loading " + dst.getName());
                        /* removed UIMessage reference from here, see FileHelperUI */
                    }

                    readSync.get(tCopy.getId()).put(_key_reading, true);
                    tCopy.start();
                    while (readSync.get(tCopy.getId()).get(_key_reading)) {
                        long time = System.currentTimeMillis();
                        fileThreads.wait(_streamsTimeout);
                        if (progress) {
                            /* removed UIMessage reference from here, see FileHelperUI */
                        }
                        if (System.currentTimeMillis() - time >= _streamsTimeout) {
                            abortedThreads.add(tCopy.getId());
                            tCopy.interrupt();
                            if (JXAenvUtils._debugSys) {
                                System.out.println("attempting a retry...");
                            }
                            if (progress) {
                                /* removed UIMessage reference from here, see FileHelperUI */
                                readProgress.get(tCopy.getId()).put(_key_read_string, "lost connection, retry attempt...");
                            }
                            retryRequested = true;
                        }
                    }
                }
                if (retryRequested) {
                    __fileCopy(src, dst, progress, id, bigBuffer);
                }
            } catch (Exception ex) {
                if (JXAenvUtils._debugSys) {
                    ex.printStackTrace();
                }
            } finally {
                if (progress) {
                    /* removed UIMessage reference from here, see FileHelperUI */
                }
            }
        }

        /**
         * secure copy of src file to dst file (first a temp. file is created
         * and filled with the data)
         *
             * @param src
             * @param dst
             * @param progress
             * @param id
             * @param bigBuffer
             * @throws java.io.FileNotFoundException
             * @throws java.io.IOException
         */
        protected static void __fileCopy(final Object src, final File dst, final boolean progress, final long id, final boolean bigBuffer) throws FileNotFoundException, IOException {
            if (JXAenvUtils._debugSys) {
                System.out.println("Local file copy");
            }
            try {
                AccessController.doPrivileged(new PrivilegedExceptionAction() {
                    public Object run() throws IOException {
                        Runnable copyRunnable = __preparefileCopy(src, dst, progress, id, bigBuffer);
                        __fileCopy_tStart(copyRunnable, src, dst, progress, id, bigBuffer);
                        return null;
                    }
                }, JXAenvUtils.acc);
            } catch (PrivilegedActionException ex) {
                throw (IOException) ex.getException();
            }
        }

            /**
             *
             * @param src
             * @param dst
             * @param progress
             * @param id
             * @param bigBuffer
             * @return
             * @throws FileNotFoundException
             * @throws IOException
             */
            public static Runnable __preparefileCopy(final Object src, final File dst, final boolean progress, final long id, boolean bigBuffer) throws FileNotFoundException, IOException {
            final int BUFFER = bigBuffer ? _BIGBUFFER_SIZE : _SMALLBUFFFER_SIZE;
            final RandomAccessFile JXAextension = src instanceof File ? new RandomAccessFile((File) src, "r") : null;
            /**
             * get the httpclient to get full information on remote connections
             */
            final HttpClient HTTPConnection = src.toString().startsWith("http:") ? new HttpClient() : null;
            final HttpMethod getData = HTTPConnection instanceof HttpClient ? new GetMethod(src.toString()) : null;
            final URLConnection connection = src instanceof URL ? ((URL) src).openConnection() : null;
            final BufferedInputStream JXAextensionStream = connection instanceof URLConnection ? new BufferedInputStream(connection.getInputStream(), BUFFER) : src instanceof BufferedInputStream ? (BufferedInputStream) src : src instanceof InputStream ? new BufferedInputStream((InputStream) src, BUFFER) : null;
            final boolean closeBufferedStream = !(src instanceof InputStream);
            if (src instanceof File) {
                if (JXAenvUtils._debugSys) {
                    System.out.println("file copy " + ((File) src).getAbsolutePath() + " -> " + dst.getAbsolutePath());
                }
                if (!_accessFilePermitted((File) src, FILE_READ)) {
                    throw new IOException("src path " + ((File) src).getAbsolutePath() + " is not readeable/doesn't exist");
                }
            } else {
                if (JXAenvUtils._debugSys) {
                    System.out.println("file copy " + (connection instanceof URLConnection ? connection.getContentLength() : JXAextensionStream.available()) + " -> " + dst.getAbsolutePath());
                }
            }
            if (dst.getParentFile() != null) {
                dst.getParentFile().mkdirs();
                _makeWritable(dst.getParentFile());
            }
            if (!_accessFilePermitted(dst.getParentFile(), FILE_WRITE | FILE_READ)) {
                throw new IOException("dst path " + dst.getAbsoluteFile().getParent() + " is not writeable");
            }
            final File tmp = _createTempFile("cp_", _TMPDIRECTORY, false);
            final RandomAccessFile JXAtmpPath = new RandomAccessFile(tmp, "rw");
            if (JXAenvUtils._debugSys) {
                System.out.print("loading ");
            }
            return new Runnable() {
                public void run() {
                    boolean dstAltered = false;
                    InputStream inputStream = JXAextensionStream;
                    if (HTTPConnection instanceof HttpClient) {
                        if (JXAenvUtils._debugSys) {
                            System.out.println("Connection with " + HTTPConnection.getClass().getCanonicalName() + " to " + src.toString());
                        }
                        getData.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(10, true));
                        int executeMethod;
                        try {
                            executeMethod = HTTPConnection.executeMethod(getData);
                            if (executeMethod != HttpStatus.SC_OK) {
                                JXAenvUtils._popExceptionToUser(false, Thread.currentThread(), new HttpException(JXAenvUtils.log("Error occured with HTTP GET connection to " + src, JXAenvUtils.LVL.APP_ERR, null)));
                            }
                            inputStream.close();
                            inputStream = getData.getResponseBodyAsStream();
                        } catch (Exception ex) {
                            if (JXAenvUtils._debugSys) {
                                ex.printStackTrace();
                            }
                            getData.releaseConnection();
                            return;
                        }
                    } else {
                        if (JXAenvUtils._debugSys) {
                            System.out.println("Connection with " + src.getClass().getCanonicalName() + " to " + src.toString());
                        }
                    }
                    byte[] b = new byte[BUFFER];
                    int r = 0;
                    int rBytes = 0;
                    try {
                        if (src instanceof File) {
                            __fileCopy_r(JXAextension, r, b, rBytes, JXAtmpPath, progress);
                        } else {
                            __fileCopy_r(inputStream, HTTPConnection, connection, r, b, rBytes, JXAtmpPath, progress);
                        }
                        JXAtmpPath.close();
                        if (src instanceof File) {
                            JXAextension.close();
                        } else if (closeBufferedStream) {
                            inputStream.close();
                        }
                        if (HTTPConnection instanceof HttpClient) {
                            getData.releaseConnection();
                        }
                        if (progress) {
                            readProgress.get(Thread.currentThread().getId()).put(_key_read_string, "copying " + dst.getName());
                        }
                        ___rawfileCopy(tmp, dst, true);
                        if (JXAenvUtils._debugSys) {
                            System.out.println("done.");
                        }
                    } catch (IOException ex) {
                        if (JXAenvUtils._debugSys) {
                            ex.printStackTrace();
                        }
                        if (dst != null) {
                            if (dst.exists() && dstAltered) {
                                dst.delete();
                            }
                        }
                        JXAenvUtils._popExceptionToUser(false, Thread.currentThread(), ex);
                    } finally {
                        if (tmp != null) {
                            if (tmp.exists()) {
                                tmp.delete();
                            }
                        }
                        synchronized (fileThreads) {
                            readSync.get(Thread.currentThread().getId()).put(_key_reading, false);
                            fileThreads.notify();
                        }
                    }
                }
            };
        }
    }
}
