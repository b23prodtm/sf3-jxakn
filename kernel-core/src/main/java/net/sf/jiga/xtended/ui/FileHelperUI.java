/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sf.jiga.xtended.ui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.Collections;
import java.util.HashMap;
import net.sf.jiga.xtended.kernel.FileHelper;
import static net.sf.jiga.xtended.kernel.FileHelper.Copy.__preparefileCopy;
import static net.sf.jiga.xtended.kernel.FileHelper.Copy._key_read_pgs_max;
import static net.sf.jiga.xtended.kernel.FileHelper.Copy._key_read_pgs_val;
import static net.sf.jiga.xtended.kernel.FileHelper.Copy._key_read_string;
import static net.sf.jiga.xtended.kernel.FileHelper.Copy._key_reading;
import static net.sf.jiga.xtended.kernel.FileHelper.Copy.readProgress;
import static net.sf.jiga.xtended.kernel.FileHelper.Copy.readSync;
import static net.sf.jiga.xtended.kernel.FileHelper._streamsTimeout;
import static net.sf.jiga.xtended.kernel.FileHelper.fileThreads;
import net.sf.jiga.xtended.kernel.JXAenvUtils;
import org.apache.commons.httpclient.HttpException;

/**
 *
 * @author wwwb23prodtminfo
 */
public class FileHelperUI {

    /**
     * secure copy of src file to dst file (first a temp. file is created and
     * filled with the data)
     *
     * @param src
     * @param bigBuffer
     * @param progressBar
     * @param dst
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void _fileCopy(File src, File dst, boolean progressBar, boolean bigBuffer) throws FileNotFoundException, IOException {
        long id = progressBar ? UIMessage.displayProgress(0, 100, null) : 0;
        __fileCopy(src, dst, progressBar, id, bigBuffer);
        if (progressBar) {
            UIMessage.kill(id);
        }
    }

    /**
     * secure copy of src file to dst file (first a temp. file is created and
     * filled with the data)
     *
     * @param src
     * @param dst
     * @param progressBar
     * @param bigBuffer
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void _fileCopy(InputStream src, File dst, boolean progressBar, boolean bigBuffer) throws FileNotFoundException, IOException {
        long id = progressBar ? UIMessage.displayProgress(0, 100, null) : 0;
        __fileCopy(src, dst, progressBar, id, bigBuffer);
        if (progressBar) {
            UIMessage.kill(id);
        }
    }

    /**
     * secure copy of src file to dst file (first a temp. file is created and
     * filled with the data)
     *
     * @param src
     * @param dst
     * @param bigBuffer
     * @param progressBar
     * @throws FileNotFoundException
     * @throws IOException
     * @throws HttpException
     */
    public static void _fileCopy(URL src, File dst, boolean progressBar, boolean bigBuffer) throws FileNotFoundException, IOException, HttpException {
        long id = progressBar ? UIMessage.displayProgress(0, 100, null) : 0;
        __fileCopy(src, dst, progressBar, id, bigBuffer);
        if (progressBar) {
            UIMessage.kill(id);
        }
    }

    /**
     * start file copy on a separate thread to allow inputstream handle and
     * progress bars display
     */
    private static void __startfileCopy(Runnable copyRunnable, Object src, File dst, boolean progressBar, long id, boolean bigBuffer) {
        boolean progressBarIndState = false;
        try {
            Thread tCopy = new Thread(copyRunnable, "file copy " + src + " -> " + dst);
            tCopy.setPriority(tCopy.MAX_PRIORITY);
            boolean retryRequested = false;
            synchronized (fileThreads) {
                readSync.put(tCopy.getId(), Collections.synchronizedMap(new HashMap<String, Boolean>(Collections.singletonMap(_key_reading, false))));
                readProgress.put(tCopy.getId(), Collections.synchronizedMap(new HashMap<String, String>()));
                readProgress.get(tCopy.getId()).put(_key_read_string, "");
                readProgress.get(tCopy.getId()).put(_key_read_pgs_val, "0");
                readProgress.get(tCopy.getId()).put(_key_read_pgs_max, "100");
                if (progressBar) {
                    progressBarIndState = UIMessage.getProgressBar(id).isIndeterminate();
                    UIMessage.getProgressBar(id).setIndeterminate(false);
                    readProgress.get(tCopy.getId()).put(_key_read_string, "loading " + dst.getName());
                }

                readSync.get(tCopy.getId()).put(_key_reading, true);
                tCopy.start();
                while (readSync.get(tCopy.getId()).get(_key_reading)) {
                    long time = System.currentTimeMillis();
                    fileThreads.wait(_streamsTimeout);
                    if (progressBar) {
                        UIMessage.getProgressBar(id).setString(readProgress.get(tCopy.getId()).get(_key_read_string));
                        UIMessage.getProgressBar(id).setMaximum(Integer.parseInt(readProgress.get(tCopy.getId()).get(_key_read_pgs_max)));
                        UIMessage.getProgressBar(id).setValue(Integer.parseInt(readProgress.get(tCopy.getId()).get(_key_read_pgs_val)));
                        UIMessage.getProgressBar(id).paintImmediately(UIMessage.getProgressBar(id).getVisibleRect());
                    }
                    if (System.currentTimeMillis() - time >= _streamsTimeout) {
                        FileHelper.abortedThreads.add(tCopy.getId());
                        tCopy.interrupt();
                        if (JXAenvUtils._debugSys) {
                            System.out.println("attempting a retry...");
                        }
                        if (progressBar) {
                            UIMessage.getProgressBar(id).setString("lost connection, retry attempt...");
                        }
                        retryRequested = true;
                    }
                }
            }
            if (retryRequested) {
                __fileCopy(src, dst, progressBar, id, bigBuffer);
            }
        } catch (Exception ex) {
            if (JXAenvUtils._debugSys) {
                ex.printStackTrace();
            }
        } finally {
            if (progressBar) {
                UIMessage.getProgressBar(id).setIndeterminate(progressBarIndState);
            }
        }
    }

    /**
     * secure copy of src file to dst file (first a temp. file is created and
     * filled with the data)
     *
         * @param src
         * @param dst
         * @param progress
         * @param id
         * @param bigBuffer
         * @throws java.io.FileNotFoundException
         * @throws java.io.IOException
     */
    public static void __fileCopy(final Object src, final File dst, final boolean progress, final long id, final boolean bigBuffer) throws FileNotFoundException, IOException {
        if (JXAenvUtils._debugSys) {
            System.out.println("Local file copy");
        }
        try {
            AccessController.doPrivileged(new PrivilegedExceptionAction() {
                public Object run() throws IOException {
                    Runnable copyRunnable = __preparefileCopy(src, dst, progress, id, bigBuffer);
                    __startfileCopy(copyRunnable, src, dst, progress, id, bigBuffer);
                    return null;
                }
            }, JXAenvUtils.acc);
        } catch (PrivilegedActionException ex) {
            throw (IOException) ex.getException();
        }

    }
}
