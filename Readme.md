<!-- toc -->

- [Introduction](#introduction)
- [History](#history)
- [Requirements](#requirements)
- [Working on a Maven Project Object Model](#Working-on-a-Maven-Project-Object-Model)
- [FAQ](#Faq-:)
  * [-bash: mvn: command not found](#-bash:-mvn:-command-not-found)
  * [Bad decrypt](#Bad-decrypt)
  * [Failed to execute goal org.sonatype.plugins](#Failed-to-execute-goal-org.sonatype.plugins)
  * [The variables GPG_KEY and GPG_PWD](#The-variables-GPG_KEY-and-GPG_PWD)
  * [How to change the default CIPHER_PWD in builds ?](#How-to-change-the-default-CIPHER_PWD-in-builds-?)
- [Opensource](#open-source)

<!-- tocstop -->

## Introduction
This is part of the SF3JSWING project, providing essential functionalities to the developer framework. Initially created to be widely spread over various projects and therefore adapted to the development environment. It usually requires some configuration, then, please, take some time to understand what it does and how to use it.
## History and main features
It was greatly inspired by a picture LRU Cache system found in the open source web. The original goal of the present API was to simplify the task to handle quite a big amount of data in the production phase. It resides in heap memory (Stack) and caches objects (Externalize) by the disk storage. As it growed, loading applets on-the-fly while supporting cross-platform code was being a central issue. A more recent feature arised to handle a WebView directly from the application to make the user able to login into social networks.
## Requirements
Minimum : Maven environment, and a Git Client.
Recommended : Java code editing tool with a Maven Project Management interface.
## Working on a Maven Project Object Model

With each artifact, `-SNAPSHOT` or final releases may be downloaded from central repositories. Also add the following into your pom.xml to specify a mirror maven central repository for dependencies :

```
# pom.xml
<repositories>
        <repository>
                <id>ossrh-snapshots</id>
                <url>https://oss.sonatype.org/content/repositories/snapshots</url>
        </repository>
        <repository>
                <id>ossrh-releases</id>
                <url>https://oss.sonatype.org/content/repositories/releases</url>
        </repository>
        <!-- some libraries get included locally -->
        <repository>
          <id>net.sf.sf3jswing.kernel</id>
          <url>file:${basedir}/lib</url>
        </repository>
</repositories>
```

With `-SNAPSHOT` the artifact stays up-to-date with nightly builds pushed out of the Continuous Integration builder. But those builds can be unstable, though tested, because of the need to reflect the latest changes in the development phase and unlike final releases, they update much more often.

The Core module collects all platform independent Java sources and resources and download from the maven tree. System specific resources are automatically profiled and got from a maven platform module.
The Platform module depends on the host operating system. It aggregates with the core module into a single package and deploys an artifact named with the classifier `-api`.

```
# pom.xml
<dependencies>
  <dependency>
      <groupId>net.sf.sf3jswing</groupId>
      <artifactId>kernel</artifactId>
      <version>1.3.19</version>
      <!-- desktop projects may download the complete api in one package -->
      <classifier>api</classifier>
  </dependency>
  <!--javafx to allow on openjdk compilers-->
  <dependency>
    <groupId>com.sun.javafx</groupId>
    <artifactId>jfxrt</artifactId>
    <version>8.0</version>
  </dependency>
</dependencies>
```
## Open source
Using other project models (apache-ant builds or simple javac applications), you can also fork and clone your own copy of the source code from GIT :
> git clone git@bitbucket.org:b23prodtm/sf3-jxakn.git

and build an artifact from Maven build, run while in the resulted clone local folder :
> mvn install

## FAQ :
### -bash: mvn: command not found
You've got to install mvn command line on Mac OS X. Use a package provider like [HomeBrew](#https://brew.sh).
> brew install maven
### Bad decrypt
```
2940:error:06065064:digital envelope routines:EVP\_DecryptFinal\_ex:bad decrypt:/SourceCache/OpenSSL098/OpenSSL098-51.2/src/crypto/evp/evp_enc.c:330:
[ERROR] Command execution failed.
```
This error was caused by an undefined XML environment variable `${env.CIPHER\_PWD} = enCrypTed`
because those files were packed and encrypted by a maven builder.

### How to define a custom environment variable like ${env.CIPHER\_PWD} ?
- With `cloud continuous integration platforms`, it is about an environment variable not set.
> CIPHER_PWD
- In Mac OS X and Linux `Terminal`, you can edit and add the following exports to the shell environment variables :

> export CIPHER_PWD="enCrypTed" CI_DEPLOY_USERNAME=b23prodtm CI_DEPLOY_PASSWORD=password >> ~/.bash_profile

* (no blank space this the default value)
- In Windows, navigate to the `System Advanced Parameters` and you should find environment variables settings for the current user profile. Then, please, restart over a new shell (Win + X type `cmd`) and the variable may be accessed through the next mvn command line.
* All you need to do is to build with maven command line from a shell that is set up with the environment variables you need.
- If you don't build from such a shell, perhaps you're running `Netbeans`. There's a workaround for that variables that used to populate `pom.xml` environment. In file `~/.m2/settings.xml` (or create the file settings.xml if it does not exist):

```
# local settings.xml
<servers>
        <server>
                <id>ossrh</id>
                <username>${env.CI_DEPLOY_USERNAME}</username>
                <password>${env.CI_DEPLOY_PASSWORD}</password>
        </server>
</servers>
<profiles>
        <profile>
                <activation>
                        <activeByDefault>true</activeByDefault>
                </activation>
                <properties>
                        <env.CIPHER_PWD>enCrypTed</env.CIPHER_PWD>
                </properties>
        </profile>
</profiles>
```

### How to change the default CIPHER_PWD in builds ?
Using the maven artefact `sf3-afile` (that you can clone from
> git clone git@bitbucket.org:b23prodtm/sf3-afile.git

Repeat step 1. and 2. of the above guidelines,  and browse to the etc/ folder) and build with your personal `CIPHER\_PWD="yourpassword"`.
Then, please, come back again to the `sf3-jxakn` maven artifact.

### Failed to execute goal org.sonatype.plugins
If you failed to deploy artifacts, that's probably because maven did an unauthorized attempt to transfer files.
Define the ossrh server in `settings.xml` :
```
<server>
  <id>ossrh</id>
  <username>${env.CI_DEPLOY_USERNAME}</username>
  <password>${env.CI_DEPLOY_PASSWORD}</password>
</server>
```
Then define the two environment variables `CI_DEPLOY_USERNAME` and `CI_DEPLOY_PASSWORD`in builds.
If you wish to deploy to releases repositories, all you need is a private gpg key :
```
<profiles>
  <profile>
    <activation>
        <activeByDefault>true</activeByDefault>
    </activation>
    <properties>
        <gpg.keyname>${env.GPG_KEY}</gpg.keyname>
        <gpg.passphrase>${env.GPG_PWD}</gpg.passphrase>
    </properties>
  </profile>
</profiles>
```

Use a command line export to setup temporary environment variables :

```
export CIPHER_PWD=enCrypTed CI_DEPLOY_USERNAME=b23prodtm CI_DEPLOY_PASSWORD=password GPG_KEY=16CBF718
mvn -V -B -s settings.xml release:prepare
mvn -V -B -s settings.xml release:perform
```

### The variables GPG_KEY and GPG_PWD
This is a secret GPG key name and password. GPG_KEY keyname must be published publicly to the new key server. Please install [GPG Suite](https://gpgtools.org) for your system to create a public key pair identity and publish it. Then export it through .circleci/private-key.gpg (don't forget to use a non-blank passphrase to protect it).

* See [FAQ](#FAQ-:).
* If you are in a Continous Integration builder, the builder's SSH public key fingerprint must be added to the git repository, to be able to use git clone.

## Author
Tiana Rakoto - b23prodtm@users.sf.net
