/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sf.jiga.xtended.kernel;

import com.apple.eawt.*;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JApplet;
import javax.swing.SwingUtilities;

/**
 *
 * @author www.b23prodtm.info
 */
public class JFCAppletApp {

        private static final Application fApplication = Application.getApplication();

        /**
         * @param args
         */
        public static void main(final String args[]) {
                SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                                try {
                                        final Class clazz = Class.forName("net.sf.jiga.xtended.ui.JFCApplet");
                                        final JApplet applet = (JApplet) clazz.getMethod("launchAppletFrame", String[].class).invoke(null, (Object[]) args);
                                        /*fApplication.addApplicationListener(new ApplicationAdapter(){
                                        
                                        @Override
                                        public void handleQuit(ApplicationEvent ae) {
                                        applet.destroy();
                                        ae.setHandled(true);
                                        System.exit(0);
                                        }
                                        
                                        @Override
                                        public void handleOpenFile(ApplicationEvent ae) {
                                        boolean ret = applet.open(ae.getFilename());
                                        ae.setHandled(ret);}
                                        
                                        }); */
                                        fApplication.setPreferencesHandler(null);
                                        fApplication.setQuitHandler(new QuitHandler() {

                                                public void handleQuitRequestWith(AppEvent.QuitEvent qe, QuitResponse qr) {
                                                        try {
                                                                clazz.getMethod("destroy").invoke(applet);
                                                        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                                                                Logger.getLogger(JFCAppletApp.class.getName()).log(Level.SEVERE, null, ex);
                                                        } finally {
                                                                qr.performQuit();
                                                        }
                                                }
                                        });
                                        fApplication.setOpenFileHandler(new OpenFilesHandler() {

                                                public void openFiles(AppEvent.OpenFilesEvent ofe) {
                                                        try {
                                                                clazz.getMethod("open", String.class).invoke(applet, ofe.getSearchTerm());
                                                        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                                                                Logger.getLogger(JFCAppletApp.class.getName()).log(Level.SEVERE, null, ex);
                                                        }
                                                }
                                        });
                                        fApplication.setOpenURIHandler(new OpenURIHandler() {

                                                public void openURI(AppEvent.OpenURIEvent ourie) {
                                                        try {
                                                                clazz.getMethod("open", String.class).invoke(applet, ourie.getURI());
                                                        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                                                                Logger.getLogger(JFCAppletApp.class.getName()).log(Level.SEVERE, null, ex);
                                                        }
                                                }
                                        });
                                        fApplication.setQuitStrategy(QuitStrategy.SYSTEM_EXIT_0);
                                } catch (ClassNotFoundException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException ex) {
                                        Logger.getLogger(JFCAppletApp.class.getName()).log(Level.SEVERE, null, ex);
                                }
                        }
                });
        }
}
